//
//  MODELSessionKeys.m
//  WOWSOME
//
//  Created by Sikandar Khan on 10/10/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MODELKeys.h"

@implementation MODELKeys


@synthesize awsAccesskey,awsSecretkey,vuforiaAccesskey,vuforiaSecretkey,vuforiaLicensekey,awsBucketName,flurryAppId,agencyId,agencyName;


@end
