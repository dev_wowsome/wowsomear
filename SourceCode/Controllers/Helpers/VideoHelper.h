/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
@protocol  VideoHelperDelegate<NSObject>
@required
-(void)mediaState;
@end

@class VCMotionPrint;

// Media states
typedef enum tagMEDIA_STATE {
    REACHED_END,
    PAUSED,
    STOPPED,
    PLAYING,
    READY,
    PLAYING_FULLSCREEN,
    NOT_READY,
    ERROR
} MEDIA_STATE;
typedef void(^OnAVPlayerInitError)();

// Used to specify that playback should start from the current position when
// calling the load and play methods
static const float VIDEO_PLAYBACK_CURRENT_POSITION = -1.0f;


// Use our own view controller instead of MPMoviePlayerViewController so we can
// control its behaviour exactly as we want it
@interface MovieViewController : UIViewController

@property (nonatomic, strong) MPMoviePlayerController* moviePlayer;

@end


@interface VideoHelper : NSObject {
    CVPixelBufferRef pixelBuffer;
    id <VideoHelperDelegate> _delegate;
@private
    // AVPlayer
    CMTime playerCursorStartPosition;
    
    // Native playback
    BOOL resumeOnTexturePlayback;
    
    // Timing
    CFTimeInterval mediaStartTime;
    CFTimeInterval playerCursorPosition;
    BOOL stopFrameTimer;
    
    // Asset
    BOOL seekRequested;
    float requestedCursorPosition;
    BOOL localFile;
    BOOL playImmediately;
    
    // Playback status
    MEDIA_STATE mediaState;
    
    // Sample and pixel buffers for video frames
    CMSampleBufferRef latestSampleBuffer;
    CMSampleBufferRef currentSampleBuffer;
    
    
    // Video properties
    CGSize videoSize;
    Float64 videoLengthSeconds;
    float videoFrameRate;
    BOOL playVideo;
    
    // Audio properties
    float currentVolume;
    BOOL playAudio;
    
    // OpenGL data
    GLuint videoTextureHandle;
    
    // Audio/video synchronisation state
    enum tagSyncState {
        SYNC_DEFAULT,
        SYNC_READY,
        SYNC_AHEAD,
        SYNC_BEHIND
    } syncStatus;
    
    // Media player type
    enum tagPLAYER_TYPE {
        PLAYER_TYPE_ON_TEXTURE,
        PLAYER_TYPE_NATIVE
    } playerType;
}
@property (nonatomic, strong) id delegate;
@property (nonatomic, retain) AVPlayerItemVideoOutput *videoOutput;
@property (readwrite, nonatomic) CVPixelBufferRef pixelBuffer;
@property (readwrite, nonatomic) VCMotionPrint *vcMotionPrint;



- (id)initWithRootViewController:(VCMotionPrint *) rootViewController  withCallBack:(OnAVPlayerInitError)givenOnAVPlayerInitError;
- (BOOL)load:(NSString*)filename playImmediately:(BOOL)playOnTextureImmediately fromPosition:(float)seekPosition;
- (BOOL)unload;
- (BOOL)isPlayableOnTexture;
- (BOOL)isPlayableFullscreen;
- (MEDIA_STATE)getStatus;
- (int)getVideoHeight;
- (int)getVideoWidth;
- (float)getLength;
- (BOOL)play:(BOOL)fullscreen fromPosition:(float)seekPosition;
- (BOOL)pause;
- (BOOL)stop;
- (BOOL)isFrameGenerated;

- (GLuint)updateVideoData:(NSString*)url;
- (void)updatePlayerCursorPosition:(float)position;
- (BOOL)seekTo:(float)position;
- (float)getCurrentPosition;
- (CMTime)getCurrentPositionAsCMTime;
- (BOOL)setVolume:(float)volume;
- (NSString*)getUrl;
- (void)setTextureId:(GLuint)textureId;
- (void)resetData;
-(void)startSampleProcess;
- (void)setShouldAutoPlay:(BOOL)autoPlay;
- (BOOL)isRenderable;
- (BOOL)isRenderable2D;
- (BOOL)isScalable;
- (BOOL)didReceiveError;
- (BOOL)playbackStarted;
-(CGSize)getVideoDimension;

@end
