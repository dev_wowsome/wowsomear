//
//  MODELMedia.h
//  Books
//
//  Created by wowdev on 10/05/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef MODELMedia_hF
#define MODELMedia_h
#import "Texture.h"
#import "MODELAction.h"
#import "VideoHelper.h"

typedef enum {
    IMAGE,
    VIDEO
} MEDIATYPE;


@interface MODELMedia : NSObject
{
    
}

@property (assign)  MEDIATYPE    mediaType;
@property (nonatomic)    NSString     *mediaId;
@property (nonatomic)    NSString     *localURL;
@property (nonatomic)    NSString     *cloudURL;
@property (nonatomic)    Texture      *texture;
@property (nonatomic)    UIImage      *videoFrame;
@property (nonatomic)    UIImage      *image;
@property (nonatomic)    NSString     *highResolutionImage;

@property (assign)  float         width;
@property (assign)  float         height;
@property (assign)  float         x;
@property (assign)  float         y;
@property (assign)  float         scale;
@property (assign)  float           z;
@property (nonatomic, assign) BOOL rendered;
@property (nonatomic, assign) int textureID;
@property (nonatomic, retain) MODELAction    *modelAction;
@property (retain, nonatomic) VideoHelper *videoHelper;
@property (assign)  GLuint          shaderID;
@property (assign)  float         actualHeight;
@property (assign)  float         actualWidth;
-(void)releaseData;



@end

#endif /* MODELMedia_h */
