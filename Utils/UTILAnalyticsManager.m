//
//  UTILAnalyticsManager.m
//  WOWSOME
//
//  Created by Sikandar Khan on 19/09/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UTILAnalyticsManager.h"
#import "Flurry.h"
#import "MODELKeys.h"

#define EVENT_NAME_CAMPAIGN_SCANNED @"evt_campaign_scanned"
#define EVENT_NAME_CAMPAIGN_ELEMENT_CTA @"evt_campaign_element_cta"
#define EVENT_NAME_CAMPAIGN_ACTIVE_TIME @"evt_campaign_active_time"
#define EVENT_NAME_CAMPAIGN_PLAYBACK_TOGGLED @"evt_campaign_playback_toggled"
#define EVENT_NAME_AR_LAUNCHED @"evt_ar_launched"

@interface UTILAnalyticsManager(){
    NSString *flurryKey;
    
}
@property (nonatomic, retain) NSString *agencyId;
@property (nonatomic, retain) NSString *agencyName;


@end


@implementation UTILAnalyticsManager



#pragma mark Singleton Methods



+(id)startSessionWithAgencyId:(NSString*)givenAgencyId agencyName:(NSString*)givenAgencyName flurryKey:(NSString*)flurryKey{
    UTILAnalyticsManager *manager = [UTILAnalyticsManager sharedInstancewithKey:flurryKey];
    manager.agencyId = givenAgencyId;
    manager.agencyName = givenAgencyName;
    
    return manager;
}

+ (id)sharedInstancewithKey:(NSString*)flurryKey {
    static UTILAnalyticsManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedMyManager = [[self alloc] initWithFlurryKey:flurryKey];
        
    });
    return sharedMyManager;
}

- (id)initWithFlurryKey:(NSString*)flurryKey{
    self = [super init];
    if (self) {
        [Flurry startSession:flurryKey];
        //[Flurry startSession:@"SHN4B9CBZG7PGPQCYMMZ"];//@"2NJ73T6NR4JBTST9ZYJW"];
        // [Flurry setCrashReportingEnabled:YES];
        // [Flurry setDebugLogEnabled:YES];
        //        NSString *sessionID=[Flurry getSessionID];
        //        NSLog(@"Session ID:::%@",sessionID);
        // NSLog(@"FLURRY INITIATED:::");
        //flurry=[Flurry sharedInstance];
        //someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
    //Flurry=nil;
}


//-----------------------------PUBLIC METHODS------------------------------

-(void)onNewCampaignScanned:(NSString*)campaignId idleTimer:(NSNumber*)idleTime{
    NSLog(@"CleverTap Campaign ID::::::::%@:::::::%@",campaignId,idleTime);
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:campaignId,@"evtprop_campaign_id",idleTime,@"evtprop_ar_idle_time",nil];
    [self recordEvent:EVENT_NAME_CAMPAIGN_SCANNED parameters:params];
}


-(void)onCampaignElementTouched:(NSString*)elementId{
    NSLog(@"CleverTap element ID::::::::%@",elementId);
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:elementId,@"evtprop_element_id", nil];
    [self recordEvent:EVENT_NAME_CAMPAIGN_ELEMENT_CTA parameters:params];
    
    
}

-(void)onCampaignViewedTimeUpdate:(NSString*)campaignId InteractionDuration:(NSNumber*)InteractionDuration LoadingDuration:(NSNumber*)LoadingDuration snapToScreenDuration:(NSNumber*)snapToScreenDuration networkStatus:(NSString*)networkStatus{
    NSLog(@"CleverTap time Update::::::campaignId::%@::::timeInForeground::%@:::networkStatus::%@:::loadingBarDuration::%@::snapToScreenDuration::%@",campaignId,InteractionDuration,networkStatus,LoadingDuration,snapToScreenDuration);
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:campaignId,@"evtprop_campaign_id",InteractionDuration,@"evtprop_interaction_duration",networkStatus,@"evtprop_network",LoadingDuration,@"evtprop_loading_duration",snapToScreenDuration,@"evtprop_snap_to_screen_duration",nil];
    [self recordEvent:EVENT_NAME_CAMPAIGN_ACTIVE_TIME parameters:params];
}

-(void)onARLaunched:(NSNumber*)InitDuration{
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:InitDuration,@"evtprop_init_duration", nil];
    [self recordEvent:EVENT_NAME_AR_LAUNCHED parameters:params];
}
-(void)onCampaignPlaybackToggled:(NSString*)campaignId action:(NSString*)action{
    NSLog(@"CleverTap Toggled::::::::%@::::::action::%@",campaignId,action);
    NSMutableDictionary *params =[NSMutableDictionary dictionaryWithObjectsAndKeys:campaignId,@"evtprop_campaign_id",action,@"evtprop_action", nil];
    [self recordEvent:EVENT_NAME_CAMPAIGN_PLAYBACK_TOGGLED parameters:params];
}


-(void)recordEvent:(NSString*)eventName parameters:(NSMutableDictionary*)params{
    
    [params setObject:_agencyId forKey:@"evtprop_agency_id"];
    [params setObject:_agencyName forKey:@"evtprop_agency_name"];
    
    FlurryEventRecordStatus num=[Flurry logEvent:eventName withParameters:params];
    NSLog(@"%@:::::%u",eventName,num);
    
    
}

-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender{
    if(!([emaiId length]==0))[Flurry setUserID:emaiId];
    if(age!=0)[Flurry setAge:age];
    if([gender length]!=0 && ([gender isEqualToString:@"m"]||[gender isEqualToString:@"f"]))[Flurry setGender:gender];
    
}

//[Flurry  addOrigin:<#(NSString *)#> withVersion:<#(NSString *)#> withParameters:<#(NSDictionary *)#>]

@end
