//
//  MODELAction.m
//  VideoPlayback
//
//  Created by wowdev on 23/06/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MODELAction.h"

@implementation MODELAction
@synthesize data1,data2;

- (void)setActionType:(int)actionType{
    if (actionType == 1) {
        _type = VISIT_URL;
    } else if (actionType == 2) {
        _type = CALL;
    } else if (actionType == 3) {
        _type = SMS;
    } else if (actionType == 4) {
        _type = MAP;
    } else if (actionType == 5) {
        _type = WHATSAPP;
    } else if (actionType == 6) {
        _type = FB_POST;
    } else if (actionType == 7) {
        _type = TWEET;
    }

}

- (ACTIONTYPE)getActionType{
    return _type;
}
@end