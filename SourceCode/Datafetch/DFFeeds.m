//
//  DFFeeds.m
//  Wowsome
//
//  Created by PIC on 14/02/16.
//  Copyright © 2016 PIC. All rights reserved.
//

#import "DFFeeds.h"

#import "WowsomePrefixHeader.pch"
#import "MODELAugment.h"
#import "MODELKeys.h"
#import "MODELResultSet.h"


@interface DFFeeds()
@property(nonatomic, strong)ResponseHandler responseHandler;
@end

@implementation DFFeeds


- (void)getMetadata:(NSString*)trackableID licenseKey:(NSString*)licenseKey  responseHandler:(ResponseHandler)responseHandler {
    
    self.responseHandler = responseHandler;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASEURL, URL_GETMETADATA];
    
    NSLog(@"Requesting metadata for %@ %@",trackableID,licenseKey);
    NSLog(@"---------");
    NSLog(@"THREAD FEEDS::::%@", [NSThread currentThread]);
    NSLog(@"---------");
    
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:trackableID, @"trackableid",licenseKey, @"licensekey",nil];
    
    
    
    HTTPClient *objHttpClient = [[HTTPClient alloc]initWithURL:url withParameters:parameters withRequestType:kPostMethod withCallBack:^(NSArray *result, id resultString, BOOL apiStatus) {
        
        
        [self processServerResponse:(NSDictionary *)resultString];
        
    } withErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
        MODELResultSet *resultSet = [[MODELResultSet alloc] initWithError:@"1" messages:@"Error in server call"];
        responseHandler(resultSet);
    }];
    
    [objHttpClient start];
    
    
}

- (void)processServerResponse:(NSDictionary*)json{
    
    
    NSLog(@"THREAD DFFEEDS processresponse %@", [NSThread currentThread]);
    
    
    MODELResultSet *resultSet = [[MODELResultSet alloc] init];
    
    [resultSet setError:[json objectForKey:@"error"]];
    [resultSet setMessage:[json objectForKey:@"message"]];
    NSLog(@"JSON:::%@",json);
    if([resultSet hasNoError]){
        
        json = [json objectForKey:@"metadata"];
        
        MODELAugment *modelAugment  = [[MODELAugment alloc] init];
        
        
        for (int i=0; i<20000; i++) {
            for (int j=0; j<20000; j++) {
                
            }
        }
        
        id          maincontent     = [json objectForKey:@"maincontent"];
        NSString    *type           = [maincontent objectForKey:@"mediatype"];
        NSString    *maincontentURL = [maincontent objectForKey:@"serverid"];
        NSString    *x              = [maincontent objectForKey:@"x"];
        NSString    *y              = [maincontent objectForKey:@"y"];
        NSString    *z              = [maincontent objectForKey:@"z"];
        NSString    *mainContentId  = [maincontent objectForKey:@"_id"];
        NSString    *width          = [maincontent objectForKey:@"width"];
        NSString    *height         = [maincontent objectForKey:@"height"];
        NSString    *scale          = [maincontent objectForKey:@"scale"];
        NSString    *maxImgVersion  = [maincontent objectForKey:@"maximgversion"];
        if(maxImgVersion==nil)maxImgVersion=@"0";
        
        MODELMedia *modelMainContent = [[MODELMedia alloc] init];
        [modelMainContent setCloudURL:maincontentURL];
        [modelMainContent setHighResolutionImage:maincontentURL];
        [modelMainContent setMediaType:(([type intValue]==1)?IMAGE:VIDEO)];
        [modelMainContent setX:[x floatValue]];
        [modelMainContent setY:[y floatValue]];
        [modelMainContent setZ:[z floatValue]];
        [modelMainContent setMediaId:mainContentId];
        [modelMainContent setWidth:[width floatValue]];
        [modelMainContent setHeight:[height floatValue]];
        [modelMainContent setScale:[scale floatValue]];
        if(modelMainContent.mediaType==IMAGE){
            [modelMainContent setCloudURL:[self getLowResolutionImageURL:modelMainContent.cloudURL availableVersions:[maxImgVersion intValue]]];
            [modelMainContent setHighResolutionImage:[self getAugmentImageURL:modelMainContent.highResolutionImage availableVersions:[maxImgVersion intValue]]];
            
            //[self getAugmentImageURL:modelMainContent.cloudURL availableVersions:[maxImgVersion intValue]]];
        }
        
        NSMutableArray  *elements       = [[NSMutableArray alloc]init];
        NSArray         *elementsJSON   = [json objectForKey:@"elements"];
        NSUInteger      elementsCount   = elementsJSON.count;
        
        for(int i=0;i<elementsCount;i++){
            
            id          elementJSON     = [elementsJSON objectAtIndex:i];
            NSString    *type           = [elementJSON objectForKey:@"mediatype"];
            NSString    *maincontentURL = [elementJSON objectForKey:@"serverid"];
            NSString    *x              = [elementJSON objectForKey:@"x"];
            NSString    *y              = [elementJSON objectForKey:@"y"];
            NSString    *z              = [elementJSON objectForKey:@"z"];
            NSString    *width          = [elementJSON objectForKey:@"width"];
            NSString    *elementId      = [elementJSON objectForKey:@"_id"];
            NSString    *height         = [elementJSON objectForKey:@"height"];
            NSString    *maxImgVersion  = [elementJSON objectForKey:@"maximgversion"];
            
            
            NSDictionary    *actionJSON = [elementJSON objectForKey:@"action"];
            NSString        *actionType = [actionJSON objectForKey:@"actiontype"];
            NSString        *actionData1 = [actionJSON objectForKey:@"data1"];
            NSString        *actionData2 = [actionJSON objectForKey:@"data2"];
            
            
            
            MODELMedia    *modelElement=[[MODELMedia alloc]init];
            [modelElement setCloudURL:[self getLowResolutionImageURL:maincontentURL availableVersions:[maxImgVersion intValue]]];
            [modelElement setHighResolutionImage:[self getAugmentImageURL:maincontentURL availableVersions:[maxImgVersion intValue]]];
            [modelElement setMediaType:(([type intValue]==1)?IMAGE:VIDEO)];
            [modelElement setWidth:[width floatValue]];
            [modelElement setHeight:[height floatValue]];
            [modelElement setMediaId:elementId];
            [modelElement setX:[x floatValue]];
            [modelElement setY:[y floatValue]];
            [modelElement setZ:[z floatValue]];
            [modelElement setZ:1.0f];
            
            MODELAction  *modelAction=[[MODELAction alloc]init];
            [modelAction setActionType:[actionType intValue]];
            [modelAction setData1:actionData1];
            [modelAction setData2:actionData2];
            [modelElement setModelAction:modelAction];
            
            [elements addObject:modelElement];
            
            
        }
        
        
        [modelAugment setMainContent:modelMainContent];
        [modelAugment setElements:elements];
        [resultSet setData:modelAugment];
    }
    
    
    
    NSLog(@"THREAD After parsing::::%@", [NSThread currentThread]);
    
    
    self.responseHandler(resultSet);
    
}

- (NSString*)getAugmentImageURL:(NSString*)url  availableVersions:(int) availableVersions{
    
    NSString    *cloudURL   = url;
    NSUInteger  location    =[cloudURL rangeOfString:@"." options:NSBackwardsSearch].location;
    NSString    *file       = [cloudURL substringToIndex:location];
    NSString    *ext        = [cloudURL substringFromIndex:location];
    
    
    NSString *postFix = nil;
    switch (availableVersions){
            
        case 4:
            postFix = @"_XH";
            break;
        case 3:
            postFix = @"_H";
            break;
        case 2:
            postFix= @"_M";
            break;
        case 1:
            postFix = @"_L";
            break;
            
    }
    
    NSString *finalurl   =    [NSString stringWithFormat:@"%@%@%@",file, postFix, ext];
    NSLog(@"High resoultion URLS%@",finalurl);
    
    return finalurl;
}


- (void)getARKeys:(NSString*)agencyId responseHandler:(ResponseHandler)responseHandler {
    
    self.responseHandler = responseHandler;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASEURL, URL_GETARKEYS];
    
    
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:agencyId, @"licensekey",nil];
    
    
    
    HTTPClient *objHttpClient = [[HTTPClient alloc]initWithURL:url withParameters:parameters withRequestType:kPostMethod withCallBack:^(NSArray *result, id resultString, BOOL apiStatus) {
        
        
        NSDictionary  *json = (NSDictionary *)resultString;
        
        
        MODELResultSet *resultSet = [[MODELResultSet alloc] init];
        
        [resultSet setError:[json objectForKey:@"error"]];
        [resultSet setMessage:[json objectForKey:@"message"]];
        
        if([resultSet hasNoError]){
            
            
            
            NSMutableArray *arKeys = [[NSMutableArray alloc] init];
            json = [json objectForKey:@"clientkeys"];
            [arKeys addObject:[json objectForKey:@"accesskey"]];
            [arKeys addObject:[json objectForKey:@"secretkey"]];
            [arKeys addObject:[json objectForKey:@"licensekey"]];
            [resultSet setData:arKeys];
            
        }
        self.responseHandler(resultSet);
        
    } withErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
        MODELResultSet *resultSet = [[MODELResultSet alloc] initWithError:@"1" messages:@"Error in server call"];
        responseHandler(resultSet);
    }];
    
    [objHttpClient start];
    
    
}
- (NSString*)getLowResolutionImageURL:(NSString*)url  availableVersions:(int) availableVersions{
    
    NSString    *cloudURL   = url;
    NSUInteger  location    =[cloudURL rangeOfString:@"." options:NSBackwardsSearch].location;
    NSString    *file       = [cloudURL substringToIndex:location];
    NSString    *ext        = [cloudURL substringFromIndex:location];
    
    
    NSString *postFix = @"_L";
    
    NSString *finalurl   =    [NSString stringWithFormat:@"%@%@%@",file, postFix, ext];
    
    NSLog(@"Low Resolution URLS %@",finalurl);
    
    return finalurl;
}




-(void)getKeys:(NSString*)agencyId responceHandler:(ResponseHandler)responseHandler{
    
    self.responseHandler = responseHandler;
    
    NSString *url = [NSString stringWithFormat:@"%@%@",BASEURL, URL_GETARKEYS];
    
    
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:agencyId, @"licensekey",nil];
    
    
    
    HTTPClient *objHttpClient = [[HTTPClient alloc]initWithURL:url withParameters:parameters withRequestType:kPostMethod withCallBack:^(NSArray *result, id resultString, BOOL apiStatus) {
        
        
        NSDictionary  *json = (NSDictionary *)resultString;
        
        NSLog(@"RESPONSE::::%@",json);
        MODELResultSet *resultSet = [[MODELResultSet alloc] init];
        
        [resultSet setError:[json objectForKey:@"error"]];
        [resultSet setMessage:[json objectForKey:@"message"]];
        
        if([resultSet hasNoError]){
            
            
            MODELKeys *modelKeys = [[MODELKeys alloc] init];
            
            
            NSDictionary *awsKeys = [json objectForKey:@"aws"];
            [modelKeys setAwsAccesskey:[awsKeys objectForKey:@"accesskey"] ];
            [modelKeys setAwsSecretkey:[awsKeys objectForKey:@"secretkey"]];
            [modelKeys setAwsBucketName:[awsKeys objectForKey:@"bucketname"] ];
            
            NSDictionary *vuforiaKeys = [json objectForKey:@"vuforia"];
            NSDictionary *clientKeys =[vuforiaKeys objectForKey:@"clientkeys"];
            [modelKeys setVuforiaAccesskey:[clientKeys objectForKey:@"accesskey"] ];
            [modelKeys setVuforiaSecretkey:[clientKeys objectForKey:@"secretkey"] ];
            [modelKeys setVuforiaLicensekey:[clientKeys objectForKey:@"licensekey"] ];
            
            NSDictionary *flurryKeys = [json objectForKey:@"flurry"];
            [modelKeys setFlurryAppId:[flurryKeys objectForKey:@"ios"] ];
            
            NSDictionary *agencyKeys = [json objectForKey:@"agency"];
            [modelKeys setAgencyId:[agencyKeys objectForKey:@"id"] ];
            [modelKeys setAgencyName:[agencyKeys objectForKey:@"name"] ];
            
            [resultSet setData:modelKeys];
            
        }
        self.responseHandler(resultSet);
        
    } withErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
        MODELResultSet *resultSet = [[MODELResultSet alloc] initWithError:@"1" messages:@"Error in server call"];
        responseHandler(resultSet);
    }];
    
    [objHttpClient start];
    
    
    
    
    
    
}


@end
