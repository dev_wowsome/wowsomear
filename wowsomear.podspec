#
#  Be sure to run `pod spec lint wowsomear.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name                    = 'wowsomear'
  s.version                 = '0.1.0'
  s.summary     	    = 'Its a very short summary just for testing'
  s.description 	    = 'This is demo description just to test the application'
  s.homepage     	    = 'https://wowsomeapp@bitbucket.org/wowsomeapp/wowsomear'
  s.license       	    = { :type => 'MIT', :file => 'LICENSE' }
  s.author       	    = { 'WOWSOME' => 'interns@wowsomeapp.com' }
  s.source                  = { :git => 'git@github.com:whatever/sdk-ios.git', :tag => '2.6.9' }
  s.source       = { :git => "https://wowsomeapp@bitbucket.org/wowsomeapp/wowsomear.git", :tag => s.version.to_s }
  
  s.ios.deployment_target = '8.0'
  s.source_files  = 'SourceCode/**/*.{h,m,mm,xib,pch,cpp,fragsh,vertsh}'

  s.frameworks = 'AVFoundation', 'MessageUI', 'CoreMotion', 'CoreMedia', 'Social', 'UIKit', 'MobileCoreServices', 'OpenGLES', 'QuartzCore', 'Security', 'SystemConfiguration', 'CoreGraphics', 'CoreFoundation', 'Foundation', 'MediaPlayer', 'AudioToolbox'

  s.xcconfig     = {
'VALID_ARCHS' => ['armv7', 'armv7s', 'arm64']
}
  s.ios.vendored_frameworks = 'Frameworks/AWSS3.framework'



  s.ios.vendored_frameworks = 'Frameworks/AWSCore.framework'

  s.ios.vendored_library = '../../build/lib/arm/libVuforia.a'

  s.ios.vendored_library    = 'Flurry/libFlurry_7.6.6.a'

  
end
