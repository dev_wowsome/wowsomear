//
//  MODELSessionKeys.h
//  WOWSOME
//
//  Created by Sikandar Khan on 10/10/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef MODELSessionKeys_h
#define MODELSessionKeys_h


#endif /* MODELSessionKeys_h */

@interface MODELKeys : NSObject


@property (nonatomic, retain)    NSString     *vuforiaAccesskey;
@property (nonatomic, retain)    NSString     *vuforiaSecretkey;
@property (nonatomic, retain)    NSString     *vuforiaLicensekey;

@property (nonatomic, retain)    NSString     *awsAccesskey;
@property (nonatomic, retain)    NSString     *awsSecretkey;
@property (nonatomic, retain)    NSString     *awsBucketName;

@property (nonatomic, retain)    NSString     *flurryAppId;

@property (nonatomic, retain)    NSString     *agencyId;
@property (nonatomic, retain)    NSString     *agencyName;



@end

