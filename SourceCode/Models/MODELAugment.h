//
//  MODELAugment.h
//  Books
//
//  Created by wowdev on 10/05/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef MODELAugment_h
#define MODELAugment_h
#import "MODELAction.h"
#import "MODELMedia.h"

#import <Foundation/Foundation.h>

typedef enum {
    AUGMENT_IMAGE,
    AUGMENT_VIDEO,
    AUGMENT_3D,
    AUGMENT_CAROUSEL
} AUGMENTTYPE;








@interface MODELAugment : NSObject
{
    
}

@property (nonatomic) NSString       *userId;
@property (nonatomic) NSString       *trackableId;
@property (nonatomic) MODELMedia     *mainContent;
@property (nonatomic) NSMutableArray *elements;
@property (assign)    BOOL           canBeRendered;
@property (assign)    BOOL           isMediaDownloaded;
@property (assign)    AUGMENTTYPE    augmentType;// TODO get rid of augment type
@property (readwrite, nonatomic)NSInteger priority;

-(void)releaseData;
- (NSMutableArray*)getMediaList;
- (NSMutableArray*)getImagesList;



@end

#endif /* MODELAugment_h */
