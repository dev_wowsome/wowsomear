//
//  WOWSOMEAR.h
//  WOWSOMEAR
//
//  Created by wowsome on 15/06/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WOWSOMEAR.
FOUNDATION_EXPORT double WOWSOMEARVersionNumber;

//! Project version string for WOWSOMEAR.
FOUNDATION_EXPORT const unsigned char WOWSOMEARVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WOWSOMEAR/PublicHeader.h>

#import <WOWSOMEAR/LibWOWSOME.h>


