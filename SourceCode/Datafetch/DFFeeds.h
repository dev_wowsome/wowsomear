//
//  DFFeeds.h
//  Wowsome
//
//  Created by PIC on 14/02/16.
//  Copyright © 2016 PIC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DFBase.h"

@interface DFFeeds : DFBase


- (void)getMetadata:(NSString*)trackableID licenseKey:(NSString*)licenseKey  responseHandler:(ResponseHandler)responseHandler;
- (void)getARKeys:(NSString*)agencyId responseHandler:(ResponseHandler)responseHandler;
- (void)getKeys:(NSString*)agencyId responceHandler:(ResponseHandler)responseHandler;

@end
