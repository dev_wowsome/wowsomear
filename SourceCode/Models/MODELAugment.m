//
//  MODELAugment.m
//  Books
//
//  Created by wowdev on 10/05/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MODELAugment.h"


@implementation MODELAugment{
    NSMutableArray *mediaList;
    NSMutableArray *imageMediaList;
    
}



@synthesize userId, trackableId, mainContent, elements, augmentType, canBeRendered, isMediaDownloaded,priority;



- (NSMutableArray*)getMediaList{
    if(mediaList==nil){
        mediaList = [[NSMutableArray alloc] initWithObjects:mainContent, nil];
        [mediaList addObjectsFromArray:elements];
    }
    
    
    return mediaList;
}

- (NSMutableArray*)getImagesList{
    if(imageMediaList==nil){
        imageMediaList = [[NSMutableArray alloc] init];
        NSMutableArray *allMediaList = [self getMediaList];
        int count = allMediaList.count;
        for (int i =0; i<count; i++) {
            MODELMedia *media  = [allMediaList objectAtIndex:i];
            if(media.mediaType == IMAGE){
                [imageMediaList addObject:media];
            }
        }
        
    }
    
    return imageMediaList;
}
-(void)releaseData{
    if(mainContent!=nil){[mainContent releaseData];mainContent=nil;}
    if(elements!=nil){
        for(id element in elements){
            if(element!=nil){
                [element releaseData];
            }
        }
        
    }
    
    trackableId=nil;
    augmentType=nil;
    canBeRendered=nil;
    isMediaDownloaded=nil;
    priority=nil;;
    
    
}




@end
