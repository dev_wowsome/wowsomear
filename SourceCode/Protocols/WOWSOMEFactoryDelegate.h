//
//  WOWSOMEFactoryDelegate.h
//  WOWSOMEAR
//
//  Created by Sikandar Khan on 19/10/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#ifndef WOWSOMEFactoryDelegate_h
#define WOWSOMEFactoryDelegate_h


#endif /* WOWSOMEFactoryDelegate_h */
#import "StateChangeListener.h"

@protocol WOWSOMEFactoryDelegate <NSObject,StateChangeListener>

@required
-(UIViewController*) getInteracativePrintController;

@required
- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener;

@required
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener;

@required
-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender;




@end



//public interface WOWSOMEFactoryDelegate {
//    
//    public WOWSOMEFactoryDelegate newInstance();
//    public Fragment getInteractivePrintFragment();
//    public void updateUserData(String email, int age, String gender);
//    public void registerStateChangeListener(StateChangeListener stateChangeListener);
//    public void unregisterStateChangeListener(StateChangeListener stateChangeListener);
//    public interface StateChangeListener{
//        public final int STATE_3D=0;
//        public final int STATE_2D=1;
//        public void onStateChanged(int state);
//    }
//}
