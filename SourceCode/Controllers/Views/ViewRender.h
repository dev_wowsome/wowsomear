/*===============================================================================
 Copyright (c) 2016 PTC Inc. All Rights Reserved.
 
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import <UIKit/UIKit.h>

#import <Vuforia/UIGLViewProtocol.h>

#import "Texture.h"
#import "SampleApplicationSession.h"
#import "VideoHelper.h"
#import "SampleGLResourceHandler.h"
#import "MODELAugment.h"
#import "StateChangeListener.h"


static const int kNumAugmentationTextures = 3;
static const int kNumVideoTargets = 1;

// VideoPlayback is a subclass of UIView and conforms to the informal protocol
// UIGLViewProtocol
@interface ViewRender : UIView <UIGLViewProtocol, SampleGLResourceHandler ,StateChangeListener> {
    
    
}




- (id)initWithFrame:(CGRect)frame rootViewController:(VCMotionPrint *) rootViewController appSession:(SampleApplicationSession *) app;



- (void) prepare:(NSString*)url;
- (BOOL) stop;
- (void) preparePlayers;
- (void) dismissPlayers;
- (void) dismiss;

- (void) finishOpenGLESCommands;
- (void) freeOpenGLESResources;

- (CGSize) getCurrentARViewBoundsSize;
- (void) compareZ:(int*)array;
- (bool) handleTouchPoint:(CGPoint) touchPoint;
- (void) setModelAugment:(MODELAugment *)newModelAugment;
- (void) updateModelAugment:(MODELAugment *)newModelAugment;
- (void) enterScanningMode;
-(void)showImage:(CVPixelBufferRef)pixelBuffer;
- (void) resetLoadingAugment;

-(void)playStatus;

-(void) FreeTexture:(GLuint)texture;
- (GLuint) createOpenGLTexture;
- (void) renderTexture:(MODELMedia*)mediaToLoad trackablePose:(Vuforia::Matrix44F) modelViewMatrix;
- (void) setLoadingBarAugment:(MODELAugment*)givenLoadingBarAugment;
- (void) showLoadingBarAugment;
- (BOOL)load:(NSString*)filename playImmediately:(BOOL)playOnTextureImmediately fromPosition:(float)seekPosition;
- (BOOL)isCurrentlyRendering;
- (void)applyAction:(MODELAction*)action;
- (void)setPlayOnFocus:(BOOL)playOnFocus;
- (void)pauseAugment;
-(void)stopPlayback;
-(NSString*)inNetworkConnection;
- (void)setTransitionToNormal;
-(void)sendAnalytics;
- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener;
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener;
-(void)publishStateChange:(int)currentState;
-(void)setIsCurrentlyRendering:(BOOL)isRendering;
- (void)setShouldAutoPlay:(BOOL)shouldAutoPlay;

@end

