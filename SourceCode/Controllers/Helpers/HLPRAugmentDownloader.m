//
//  HLPRImageDownloader.m
//  VideoPlayback
//
//  Created by wowdev on 30/06/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HLPRAugmentDownloader.h"
#import "MODELAugment.h"
#import "AsyncImageDownloader.h"
#import "MODELKeys.h"
@interface HLPRAugmentDownloader (){
    
    MODELKeys *keys;
}
@property (nonatomic) void (^successCallback)(MODELAugment *modelAugment);
@property (nonatomic) void (^failCallback)(MODELAugment *modelAugment);
@property (nonatomic, retain) MODELAugment *modelAugment;
@property (nonatomic, retain) NSMutableArray   *mediaListToDownload;

@end

@implementation HLPRAugmentDownloader



- (id)initWithAugment:(MODELAugment*)augment modelKeys:(MODELKeys*)modelKeys successBlock:(void (^)(MODELAugment *modelAugment))success failBlock:(void(^)(MODELAugment *modelAugment))fail{
    self = [super init];
    _modelAugment = augment;
    _successCallback = success;
    keys=modelKeys;
    _failCallback = fail;
    //_dlgAugmentDownloader = givenDelegate;
    return self;
}



- (void) startDownload {
    @try {
        if (_modelAugment == nil) {
            
            if (_failCallback != nil)
                _failCallback(_modelAugment);
            
        } else {
            
            MODELMedia *trackableMedia = _modelAugment.mainContent;
            if(trackableMedia.mediaType == VIDEO && trackableMedia.cloudURL != nil){
                
                UIImage *videoFrame= [self generateThumbImage : trackableMedia.cloudURL];
                [trackableMedia setVideoFrame:videoFrame];
                
            }
            
            if (trackableMedia.mediaType == IMAGE && trackableMedia.cloudURL != nil && trackableMedia.cloudURL.length>0) {
                
                if (_mediaListToDownload == nil) _mediaListToDownload = [[NSMutableArray alloc] init];
                [_mediaListToDownload addObject:trackableMedia];
            }
            
            //            MODELMedia *mainContentMedia = _modelAugment.mainContent;
            //            if (mainContentMedia.mediaType == IMAGE && mainContentMedia.cloudURL != nil && mainContentMedia.cloudURL.length>0) {
            //
            //                if (_mediaListToDownload == nil) _mediaListToDownload = [[NSMutableArray alloc] init];
            //                [_mediaListToDownload addObject:mainContentMedia];
            //            }
            //
            
            
            NSMutableArray *elementsMediaList = _modelAugment.elements;
            if (elementsMediaList != nil && elementsMediaList.count > 0) {
                
                for (MODELMedia *elementMedia in elementsMediaList) {
                    if (elementMedia.mediaType == IMAGE && elementMedia.cloudURL != nil && elementMedia.cloudURL.length>0) {
                        
                        if (_mediaListToDownload == nil) _mediaListToDownload = [[NSMutableArray alloc] init];
                        [_mediaListToDownload addObject:elementMedia];
                    }
                }
            }
            
            if (_mediaListToDownload != nil && _mediaListToDownload.count > 0) {
                for (MODELMedia *modelMedia in _mediaListToDownload) {
                    AsyncImageDownloader *downloader= [[AsyncImageDownloader alloc] initWithMediaURL:modelMedia.cloudURL modelKeys:keys successBlock:^(UIImage *image) {
                        
                        [modelMedia setImage:image];
                        modelMedia.actualWidth=image.size.width;
                        modelMedia.actualHeight=image.size.height;
                        
                        NSLog(@"Success DOWNLODING cloud:%@",modelMedia.cloudURL);
                        [self checkIfDownloadCompletedAndReturn:modelMedia];
                        
                    } failBlock:^(NSError *error) {
                        NSLog(@"FAILED DOWNLODING cloud: %@",modelMedia.cloudURL);
                        [self checkIfDownloadCompletedAndReturn:modelMedia];
                    }];
                    [downloader startDownload];
                    
                    AsyncImageDownloader *highResolutionImageDownloader= [[AsyncImageDownloader alloc]initWithMediaURL:modelMedia.highResolutionImage modelKeys:keys successBlock:^(UIImage *reoultionimage) {
                        [modelMedia setImage:reoultionimage];
                        
                        
                        NSLog(@"Success DOWNLODING:%@",modelMedia.highResolutionImage);
                        // [self checkIfDownloadCompletedAndReturn:modelMedia];
                        
                    } failBlock:^(NSError *error) {
                        NSLog(@"FAILED DOWNLODING:%@",modelMedia.highResolutionImage);
                        //  [self checkIfDownloadCompletedAndReturn:modelMedia];
                    }];
                    [highResolutionImageDownloader startDownload];
                    
                    
                    
                }
                
            } else {
                if (_successCallback != nil) _successCallback(_modelAugment);
                
            }
            
            
        }
    }@catch(NSException* e){
        NSLog(@"Augment download failed %@", e.debugDescription);
        if(_failCallback!=nil)_failCallback(_modelAugment);
    }
    
}



- (void) checkIfDownloadCompletedAndReturn:(MODELMedia*) modelMedia{
    [_mediaListToDownload removeObject:modelMedia];
    
    if(_mediaListToDownload.count==0){
        _modelAugment.isMediaDownloaded =YES;
        if (_successCallback != nil) _successCallback(_modelAugment);
        _mediaListToDownload     =   nil;
        _successCallback    =   nil;
        _failCallback    =   nil;
        _modelAugment =nil;
    }
}

-(UIImage *)generateThumbImage : (NSString *)url{
    NSURL *URL = [NSURL URLWithString:url];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:URL options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
    return FrameImage;
    //    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:URL];
    //    UIImage *thumbnail = [player thumbnailImageAtTime:52.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    //    [player stop];
    //    return thumbnail;
}

@end
