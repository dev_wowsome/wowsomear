/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Texture : NSObject {
@private
    int channels;
}


// --- Properties ---


@property (nonatomic, assign) int width;
@property (nonatomic, assign) int height;
@property (nonatomic) UIImage  *image;
@property (nonatomic, readonly) unsigned char* pngData;


// --- Public methods ---
- (id)initWithImageFile:(NSString*)filename;

- (id)initWithUIImage:(UIImage*)image;

@end
