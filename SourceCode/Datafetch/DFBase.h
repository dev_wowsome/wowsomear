//
//  DFBase.h
//  VideoPlayback
//
//  Created by wowsome on 13/07/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef DFBase_h
#define DFBase_h


#endif /* DFBase_h */

#import "HTTPClient.h"
#import "MODELResultSet.h"

typedef void(^ResponseHandler) (MODELResultSet *resultSet);

@interface DFBase : NSObject

@end