//
//  WSWebServiceManager.m
//  Wowsome
//
//  Created by PIC on 04/02/16.
//  Copyright © 2016 PIC. All rights reserved.
//

#import "HTTPClient.h"

@interface HTTPClient () {
    NSString *url;
    NSDictionary *parameters;
    
    SuccessHandler successHandler;
    ErrorHandler errorHandler;
    
    RequestType requestType;
}

@end

@implementation HTTPClient

- (id)initWithURL:(NSString *)givenUrl withParameters:(NSDictionary *)givenParameters withRequestType:(RequestType)givenRequestType withCallBack:(SuccessHandler)givenSuccessHandler withErrorHandler:(ErrorHandler) givenErrorHandler {
    self = [super init];
    
    successHandler = givenSuccessHandler;
    errorHandler = givenErrorHandler;
    url = givenUrl;
    parameters = givenParameters;
    requestType = givenRequestType;
    
    return self;
}

- (void)start {
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    
    if (requestType == kPostMethod) {
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            successHandler(nil, responseObject, YES);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            errorHandler(error, @"", YES);
        }];
    }
    else {
        
        [manager GET:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"%@", responseObject);
            successHandler(nil, responseObject, YES);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@", error);
            errorHandler(error, @"", YES);
        }];
    }
}

@end