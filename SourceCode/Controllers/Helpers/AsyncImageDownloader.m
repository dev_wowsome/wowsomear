/*
 * Copyright (c) 2013 Kyle W. Banks
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
//  AsyncImageDownloader.m
//
//  Created by Kyle Banks on 2012-11-29.
//  Modified by Nicolas Schteinschraber 2013-05-30
//
#import <AWSS3/AWSS3.h>
#import "WowsomePrefixHeader.pch"
#import "AsyncImageDownloader.h"
#import "MODELKeys.h"

@interface AsyncImageDownloader(){
    MODELKeys *keys;
    
}

@end
@implementation AsyncImageDownloader

NSInteger retryCount = 0;

@synthesize mediaURL, fileURL;

-(id)initWithMediaURL:(NSString *)theMediaURL modelKeys:(MODELKeys*)modelKeys successBlock:(void (^)(UIImage *image))success failBlock:(void(^)(NSError *error))fail
{
    self = [super init];
    
    if(self)
    {
        [self setMediaURL:theMediaURL];
        [self setFileURL:nil];
        successCallback = success;
        keys= modelKeys;
        failCallback = fail;
    }
    
    return self;
}

-(id)initWithFileURL:(NSString *)theFileURL successBlock:(void (^)(NSData *data))success failBlock:(void(^)(NSError *error))fail
{
    self = [super init];
    
    if(self)
    {
        
        [self setMediaURL:nil];
        [self setFileURL:theFileURL];
        successCallbackFile = success;
        failCallback = fail;
    }
    
    return self;
}

//Perform the actual download
-(void)startDownload
{
    
    // [AsyncImageDownloader updateURLForAWSAccess:mediaURL awsTask: ^id(AWSTask *task) {
    [AsyncImageDownloader updateURLForAWSAccess:mediaURL modelKeys:keys awsTask: ^id(AWSTask *task){
        if (task.error) {
            NSLog(@"AWS Error: %@",task.error);
        } else {
            fileData = [[NSMutableData alloc] init];
            NSURL *finalURL = task.result;
            //    mediaURL = finalURL.absoluteString;
            
            
            //                NSURL *url=@"https://s3-ap-southeast-1.amazonaws.com/wowsomedevcontent/2016_33/e_57975c08b3aa1cd35307b62a_1471589617_0_H.png";
            //                NSString *medURL = url.absoluteString;
            //
            //                if([mediaURL isEqualToString:medURL])
            //                {
            //                    finalURL = url;
            //
            //                }
            //                else{
            //                    finalURL=finalURL;
            //                }
            
            
            NSURLRequest *request = [NSURLRequest requestWithURL:finalURL];
            
            NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
            if(!connection)
            {
                if(retryCount==3)
                {
                    failCallback([NSError errorWithDomain:@"Failed to create connection" code:0 userInfo:nil]);
                }
                else{
                    [self retry];
                }
            }else{
                [connection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                                      forMode:NSDefaultRunLoopMode];
                [connection start];
                
            }
        }
        return nil;
    }];
    
    
}

#pragma mark NSURLConnection Delegate
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    failCallback(error);
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if([response respondsToSelector:@selector(statusCode)])
    {
        int statusCode = [((NSHTTPURLResponse *)response) statusCode];
        if (statusCode >= 400)
        {
            [connection cancel];
            if(retryCount==3){
                failCallback([NSError errorWithDomain:@"Image download failed due to bad server response" code:0 userInfo:nil]);}
            else{
                [self retry];}
        }
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [fileData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    if(fileData == nil)
    {
        if(retryCount==3){
            failCallback([NSError errorWithDomain:@"No data received" code:0 userInfo:nil]);}
        else{
            [self retry];}
    }
    else
    {
        if (fileURL) {
            successCallbackFile(fileData);
        } else {
            UIImage *image = [UIImage imageWithData:fileData];
            
            successCallback(image);
        }
    }
}

+ (void)updateURLForAWSAccess:(NSString*)awsFileName modelKeys:(MODELKeys*)modelKeys awsTask:(AWSContinuationBlock)block{
    
    
    @try {
        
        NSString *AWSSecretKey = AWSSecretKey;
        
        //        AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:AMAZONACCESSKEY secretKey:AMAZONSECRETKEY];
        AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:modelKeys.awsAccesskey secretKey:modelKeys.awsSecretkey];
        
        
        AWSServiceConfiguration *serviceConfiguration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionAPSoutheast1 credentialsProvider:credentialsProvider];
        serviceConfiguration.maxRetryCount = 10;
        
        //((deprecated("Use '+ registerS3PreSignedURLBuilderWithConfiguration:forKey:' and '+ S3PreSignedURLBuilderForKey:' instead.")));
        
        [AWSS3PreSignedURLBuilder registerS3PreSignedURLBuilderWithConfiguration:serviceConfiguration forKey:@"APSoutheast1S3PreSignedURLBuilder"];
        AWSS3PreSignedURLBuilder *builder = [AWSS3PreSignedURLBuilder S3PreSignedURLBuilderForKey:@"APSoutheast1S3PreSignedURLBuilder"];
        
        AWSS3GetPreSignedURLRequest *request = [[AWSS3GetPreSignedURLRequest alloc] init];
        request.bucket = modelKeys.awsBucketName;
        request.key = awsFileName;
        request.HTTPMethod = AWSHTTPMethodGET;
        request.expires = [NSDate dateWithTimeIntervalSinceNow:15000];
        //request.contentType = contentType;
        
        
        [[builder getPreSignedURL:request] continueWithBlock:block];
        
    }@catch (NSException *e){
        NSLog(@"getAWSUpdatedURL Error %@",e.description);
    }
}
-(void)retry{
    if(retryCount<=3)
    {
        [self startDownload];
        NSLog(@"RETRY::::::%i",retryCount);
        retryCount++;
    }else{return;}
}
@end
