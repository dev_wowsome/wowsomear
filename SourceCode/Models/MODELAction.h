//
//  MODELAction.h
//  VideoPlayback
//
//  Created by wowdev on 23/06/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef MODELAction_h
#define MODELAction_h

typedef enum {
    VISIT_URL =1,
    CALL,
    SMS,
    MAP,
    WHATSAPP,
    FB_POST,
    TWEET
} ACTIONTYPE;

@interface MODELAction : NSObject
{
    
}
@property (assign)    ACTIONTYPE      type;
@property (nonatomic)    NSString     *data1;
@property (nonatomic)    NSString     *data2;

- (void)setActionType:(int)actionType;
- (ACTIONTYPE)getActionType;

@end

#endif /* MODELAction_h */
