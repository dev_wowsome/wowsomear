//
//  MODELResultSet.m
//  VideoPlayback
//
//  Created by wowsome on 13/07/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MODELResultSet.h"


@interface MODELResultSet()
    
@end



@implementation MODELResultSet

@synthesize error, message, data;

- (id) init {
    
    return [self initWithError:@"0" messages:@""];
}


- (id) initWithError:(NSString*) error messages:(NSString*)message {
    
    self = [super init];
    self.error = error;
    self.message = message;
    
    return self;
}



- (NSString*)getMessage {
    if(self.message==nil || self.message.length==0)self.message =@"Message not set";
    return self.message;
}


- (NSString*) getDoubleLineUIMessage {
    return [NSString stringWithFormat:@"%@\n\nCode: %@", self.message, self.error];
}

- (NSString*) getSingleLineUIMessage {
    return [NSString stringWithFormat:@"Code %@. %@", self.error, self.message];
}



- (BOOL)hasError {
    return ![self.error isEqualToString:@"0"];
}

- (BOOL)hasNoError{
    return ![self hasError];
}



@end