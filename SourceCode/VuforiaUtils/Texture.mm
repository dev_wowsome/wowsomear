/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import "Texture.h"
#import <UIKit/UIKit.h>


// Private method declarations
@interface Texture (PrivateMethods)
- (BOOL)loadImage:(NSString*)filename;
- (BOOL)copyImageDataForOpenGL:(CFDataRef)imageData;
@end


@implementation Texture
@synthesize width, height,image;

//------------------------------------------------------------------------------
#pragma mark - Lifecycle

- (id)initWithImageFile:(NSString*)filename{
    self = [super init];
    
    if (nil != self) {
        if (NO == [self loadImage:filename]) {
            NSLog(@"Failed to load texture image from file %@", filename);
            self = nil;
        }
    }
    
    return self;
}


- (id)initWithUIImage:(UIImage*)image{
    self = [super init];
    
    if (nil != self) {
        if (NO == [self retrieveImageData:image]) {
            NSLog(@"Failed to load texture image");
            self = nil;
        }
    }
    
    return self;
}

- (void)dealloc{
    if (_pngData) {
        delete[] _pngData;
    }
}


//------------------------------------------------------------------------------
#pragma mark - Private methods

- (BOOL)loadImage:(NSString*)filename{
    BOOL ret = NO;
    
    // Build the full path of the image file
    NSString* fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    // Create a UIImage with the contents of the file
    UIImage* uiImage = [UIImage imageWithContentsOfFile:fullPath];
    
    if (uiImage) {
        // Get the inner CGImage from the UIImage wrapper
        CGImageRef cgImage = uiImage.CGImage;
        
        // Get the image size
        width = (int)CGImageGetWidth(cgImage);
        height = (int)CGImageGetHeight(cgImage);
        
        // Record the number of channels
        channels = (int)CGImageGetBitsPerPixel(cgImage)/CGImageGetBitsPerComponent(cgImage);
        
        // Generate a CFData object from the CGImage object (a CFData object represents an area of memory)
        CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
        
        // Copy the image data for use by Open GL
        ret = [self copyImageDataForOpenGL: imageData];
        NSLog(@"Image LOADED");
        
        CFRelease(imageData);
    }
    
    return ret;
}

- (BOOL)retrieveImageData:(UIImage*)uiImage{
    
    BOOL ret = NO;
    
    image=uiImage;
    // Get the inner CGImage from the UIImage wrapper
    CGImageRef cgImage = uiImage.CGImage;
    
    // Get the image size
    width = (int)CGImageGetWidth(cgImage);
    height = (int)CGImageGetHeight(cgImage);
    
    // Record the number of channels
    channels = (int)CGImageGetBitsPerPixel(cgImage)/CGImageGetBitsPerComponent(cgImage);
    
    // Generate a CFData object from the CGImage object (a CFData object represents an area of memory)
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    // Copy the image data for use by Open GL
    ret = [self copyImageDataForOpenGL: imageData];
    
    CFRelease(imageData);
    
    return ret;
}

- (BOOL)copyImageDataForOpenGL:(CFDataRef)imageData{
    if (_pngData) {
        delete[] _pngData;
    }
    
    _pngData = new unsigned char[width * height * channels];
    const int rowSize = width * channels;
    const unsigned char* pixels = (unsigned char*)CFDataGetBytePtr(imageData);
    
    // Copy the row data from bottom to top
    for (int i = 0; i < height; ++i) {
        memcpy(_pngData + rowSize * i, pixels + rowSize * (height - 1 - i), width * channels);
    }
    
    return YES;
}
@end
