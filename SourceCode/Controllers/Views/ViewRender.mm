/*===============================================================================
 Copyright (c) 2016 PTC Inc. All Rights Reserved.
 
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <sys/time.h>

#import <Vuforia/Vuforia.h>
#import <Vuforia/State.h>
#import <Vuforia/Tool.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/TrackableResult.h>
#import <Vuforia/ImageTarget.h>
#import <Vuforia/ImageTargetResult.h>
#import <Vuforia/TrackerManager.h>
#import <Vuforia/ObjectTracker.h>
#import <Vuforia/TargetFinder.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "ViewRender.h"
#import "Texture.h"
#import "SampleApplicationUtils.h"
#import "SampleApplicationShaderUtils.h"
#import "Teapot.h"
#import "VCMotionPrint.h"
#import "Transition3Dto2D.h"
#import "MODELAugment.h"
#import "MODELMedia.h"
#import "VideoHelper.h"
#import "SampleMath.h"
#import "Quad.h"
#import "DFFeeds.h"
#import <Social/Social.h>
#import "HLPRAugmentDownloader.h"
#import "UTILAnalyticsManager.h"
#import "Reachability.h"
#import "VuforiaHelper.h"


//******************************************************************************
// *** OpenGL ES thread safety ***
//
// OpenGL ES on iOS is not thread safe.  We ensure thread safety by following
// this procedure:
// 1) Create the OpenGL ES context on the main thread.
// 2) Start the Vuforia camera, which causes Vuforia to locate our EAGLView and start
//    the render thread.
// 3) Vuforia calls our renderFrameVuforia method periodically on the render thread.
//    The first time this happens, the defaultFramebuffer does not exist, so it
//    is created with a call to createFramebuffer.  createFramebuffer is called
//    on the main thread in order to safely allocate the OpenGL ES storage,
//    which is shared with the drawable layer.  The render (background) thread
//    is blocked during the call to createFramebuffer, thus ensuring no
//    concurrent use of the OpenGL ES context.
//
//******************************************************************************

static int RS_NORMAL = 0;
static int RS_TRANSITION_TO_2D = 1;
static int RS_TRANSITION_TO_3D = 2;
static int RS_SCANNING = 3;
static int RS_OVERLAY = 4;
namespace {
    // --- Data private to this unit ---
    
    // const NSTimeInterval TRACKING_LOST_TIMEOUT      = 0.0f;
    const GLfloat       videoQuadTextureCoords[]    = { 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0};// Video quad texture coordinates
    
    
    
    VCMotionPrint               *vcMotionPrint;//TODO usage to be reviewd and can be removed
    MODELAugment                *tempAugmentToRender;
    MODELAugment                 *currentAugmentToRender;// Instantiate one VideoHelper per target
    MODELAugment                   *loadingBarAugment;
    MODELKeys                       *modelKeys;
    SampleApplicationSession    *vapp;
    
    
    
    float                       trackableWidth;
    float                       trackableHeight;
    NSString*                   currentTargetID=nil;
    NSString*                   state;
    long                        timer;
    NSString *                  uniqueTargetId;
    long                        augmentDuration;
    long                        loadingDuration;
    long                        idleTimer;
    long                         snapToScreen;

    
    Vuforia::Matrix44F          modelViewMatrix; //TODO can be removed
    NSTimer                     *trackingLostTimer;// Timer to pause on-texture video playback after tracking has been lost.
    // Note: written/read on two threads, but never concurrently
    NSLock*                     dataLock;// Lock to synchronise data that is (potentially) accessed concurrently
    EAGLContext                 *context;// OpenGL ES context
    
    // The OpenGL ES names for the framebuffer and renderbuffers used to render to this view
    GLuint                      defaultFramebuffer;
    GLuint                      colorRenderbuffer;
    GLuint                      depthRenderbuffer;
    
    // Shader handles
    GLuint                      loadingBarShaderID;
    GLuint                      shaderID;
    GLint                       vertexHandle;
    GLint                       normalHandle;
    GLint                       textureCoordHandle;
    GLint                       mvpMatrixHandle;
    GLint                       texSampler2DHandle;
    
    
    // VARIABLES BELEIVED THT CAN BE REMOVED
    BOOL                        isActive;//TODO checked teh usage , can be removed
    
    Vuforia::Vec2F              trackableSize;
    const Vuforia::TrackableResult  *trackableResult;
    //  BOOL                        isCurrentlyRendering;
    NSMutableArray                     *stateChangeListeners;
    BOOL                        isCurrentlyRendering;
    BOOL                        closedWithAugmentPaused;
    BOOL                        elementTouched;
    
}


@interface ViewRender ()<MFMessageComposeViewControllerDelegate>


@property (nonatomic, assign) GLuint trackingTextureID;
@property (nonatomic, strong) NSMutableArray *objects3D;  // objects to draw
@property (nonatomic, readwrite) BOOL trackingTextureIDSet;
@property (nonatomic, readwrite) BOOL isCurrentlyRendering;

@property (nonatomic, readwrite) BOOL trackingTextureAvailable;
@property (nonatomic, readwrite) BOOL isViewingTarget;
@property (nonatomic, readwrite) BOOL isShowing2DOverlay;
@property (nonatomic, retain)  MODELAugment     *modelAugmentToRender;
@property (nonatomic, retain) MODELAugment      *currentAugmentToRender;
@property (nonatomic, readwrite) BOOL      shouldAutoPlay;
@property (nonatomic, readwrite) BOOL playOnFocus;

// ----------------------------------------------------------------------------
// 3D to 2D Transition control variables
// ----------------------------------------------------------------------------
@property (nonatomic, assign) Transition3Dto2D* transition3Dto2D;
@property (nonatomic, assign) Transition3Dto2D* transition2Dto3D;

@property (nonatomic, readwrite) BOOL startTransition;
@property (nonatomic, readwrite) BOOL startTransition2Dto3D;
@property (nonatomic, readwrite) BOOL reportedFinished;

@property (nonatomic, readwrite) BOOL reportedFinished2Dto3D;

@property (nonatomic, readwrite) int renderState;
@property (nonatomic, readwrite) float transitionDuration;

// Lock to prevent concurrent access of the framebuffer on the main and
// render threads (layoutSubViews and renderFrameVuforia methods)
@property (nonatomic, strong) NSLock *framebufferLock;

// Lock to synchronise data that is (potentially) accessed concurrently
@property (nonatomic, strong) NSLock* dataLock;

@property (nonatomic, readwrite) BOOL mDoLayoutSubviews;
// Whether the application is in scanning mode (or in content mode):
@property (nonatomic, readwrite) bool scanningMode;



- (void)initShaders;
- (void)createFramebuffer;
- (void)deleteFramebuffer;
- (void)setFramebuffer;
- (BOOL)presentFramebuffer;

@end

@implementation ViewRender 





// You must implement this method, which ensures the view's underlying layer is
// of type CAEAGLLayer
+ (Class)layerClass{
    return [CAEAGLLayer class];
}


//-------------------------------View Life cycle methods------------------------------------------
#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame rootViewController:(VCMotionPrint *) rootViewController appSession:(SampleApplicationSession *) app{
    self = [super initWithFrame:frame];
    
    if (self) {
        vapp = app;
        _scanningMode = YES;
        _isViewingTarget = NO;
        _trackingTextureAvailable = NO;
        self.renderState = RS_OVERLAY;
        
        vcMotionPrint = rootViewController;
        
        // Enable retina mode if available on this device
        if (YES == [vapp isRetinaDisplay]) {
            [self setContentScaleFactor:[UIScreen mainScreen].nativeScale];
        }
        
        _playOnFocus=NO;
        [self setShouldAutoPlay:YES];
        
        
        
        // Create the OpenGL ES context
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        
        // The EAGLContext must be set for each thread that wishes to use it.
        // Set it the first time this method is called (on the main thread)
        if (context != [EAGLContext currentContext]) {
            [EAGLContext setCurrentContext:context];
        }
        
        
        
        
        [self initShaders];
        //[self loadVideoHelper];
        
        
    }
    
    return self;
}

- (void)layoutSubviews{
    self.mDoLayoutSubviews = YES;
    
}

- (void)doLayoutSubviews{
    // this method will be called during the rotation of the device
    // if we are in the middle of the 3D to 2D rotation, it will be aborted
    // so we need to finish the transition and put in the app in a proper state
    // otherwise we wouldn't display the book overlay as expected
    if (self.renderState == RS_TRANSITION_TO_2D) {
        [self endTransitionOnTargetLost];
    }
    
    // The framebuffer will be re-created at the beginning of the next setFramebuffer method call.
    [self deleteFramebuffer];
    
    // Initialisation done once, or once per screen size change
    [self initRendering];
    
    
}

- (void)initRendering{
    // TODO: we are forcing here the animation to be in portrait
    self.transition3Dto2D = new Transition3Dto2D(self.frame.size.width, self.frame.size.height, YES);
    self.transition3Dto2D->initializeGL(shaderID);
    
    self.transition2Dto3D = new Transition3Dto2D(self.frame.size.width, self.frame.size.height, YES);
    self.transition2Dto3D->initializeGL(shaderID);
    
    self.renderState = RS_NORMAL;
    
    self.transitionDuration = 0.5f;
    trackableSize = Vuforia::Vec2F(0.0f, 0.0f);
}

- (void) setOrientationTransform:(CGAffineTransform)transform withLayerPosition:(CGPoint)pos {
    self.layer.position = pos;
    self.transform = transform;
}

- (void) endTransitionOnTargetLost {
    self.isShowing2DOverlay = YES;
    self.startTransition = NO;
    
    self.renderState = RS_NORMAL;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kTargetLost" object:nil userInfo:nil];
    
    //[vcMotionPrint presentViewController:_objFeedBlowUpVC animated:NO completion:nil];
    
    [self updateModelAugment:tempAugmentToRender];
    //        MODELMedia *mediaToLoad = modelAugmentToRender.mainContent;
    //        float scaleWidth    = trackableWidth * mediaToLoad.width;
    //        float scaleHeight   = trackableHeight * mediaToLoad.height;
    //        CMTime seekTime = [videoPlayerHelper getCurrentPositionAsCMTime];
    //        [_objFeedBlowUpVC setModelAugment:modelAugmentToRender seekTime:seekTime width:scaleWidth height: scaleHeight];
}


-(void)updateModelAugment:(MODELAugment*)newAugment{
    if(newAugment!=tempAugmentToRender)
    {
        [self setModelAugment:newAugment];
    }
    //        MODELMedia *media=newAugment.mainContent;
    //        float scaleWidth    = trackableWidth * media.width;
    //        float scaleHeight   = trackableHeight * media.height;
    //        CMTime seekTime = [videoPlayerHelper getCurrentPositionAsCMTime];
    // [_objFeedBlowUpVC setModelAugment:modelAugmentToRender seekTime:seekTime width:scaleWidth height: scaleHeight];
    
}

- (void)targetLost{
    if ((self.renderState == RS_NORMAL) || (self.renderState == RS_TRANSITION_TO_3D)|| (self.renderState == RS_OVERLAY)|| (self.renderState == RS_SCANNING))
    {
        self.transitionDuration = 0.5f;
        //When the target is lost starts the 3d to 2d Transition
        self.renderState = RS_TRANSITION_TO_2D;
        self.startTransition = YES;
        [vcMotionPrint addButtons];
        [vcMotionPrint scanlineStop];
        snapToScreen =[vcMotionPrint setTimer];
        
        
    }
    
    self.isViewingTarget = NO;
    // [self enterContentMode];
}

- (void)targetReacquired{
    if ((self.renderState == RS_NORMAL && self.isShowing2DOverlay) || self.renderState == RS_TRANSITION_TO_2D || self.renderState == RS_OVERLAY)
    {
        
        self.renderState = RS_TRANSITION_TO_3D;
        self.startTransition2Dto3D = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kTargetReacquired" object:nil userInfo:nil];
        [vcMotionPrint removeButtons];
        loadingDuration = 0;
    }
}


- (void)onCloseOverlayCalled{
    //todo show scanline
}


- (void) enterContentMode{
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    if (objectTracker != NULL) {
        Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
        
        if (targetFinder != NULL) {
            // Stop visual search
            [vcMotionPrint setVisualSearchOn:!targetFinder->stop()];
            
            // Remember we are in content mode
            _scanningMode = NO;
        }
    }
    else {
        NSLog(@"Failed to enter content mode: ObjectTracker is NULL.");
    }
}

- (void) enterScanningMode{
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if (objectTracker != NULL) {
        Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
        if (targetFinder != NULL) {
            // Start visual search
            [vcMotionPrint setVisualSearchOn:targetFinder->startRecognition()];
            
            // Clear all trackables created previously
            targetFinder->clearTrackables();
        }
        
        _scanningMode = YES;
        
        self.isViewingTarget = NO;
        self.renderState = RS_SCANNING;
        self.isShowing2DOverlay = NO;
    }
    else {
        NSLog(@"Failed to enter scanning mode: ObjectTracker is NULL.");
    }
}


// -----------------------PUBLIC METHODS---------------------------

- (void) setModelAugment:(MODELAugment *)newModelAugment{
    if(newModelAugment!=nil){
        loadingDuration=0;
        
        newModelAugment.mainContent.shaderID=shaderID;
        NSLog(@"Augment set %@  self::%@",newModelAugment.mainContent.cloudURL,self);
        if(newModelAugment!=nil){
            NSLog(@"THread ::::render::::%@", [NSThread currentThread]);
            
            
            
            // if(modelAugmentToRender==nil){
            if(newModelAugment.mainContent.mediaType==VIDEO && newModelAugment.mainContent.videoHelper==nil){
                [self setShouldAutoPlay:YES];
                
                
                VideoHelper *newHelper=[[VideoHelper alloc]initWithRootViewController:vcMotionPrint withCallBack:nil];
                NSLog(@"VideoHelper Created in setmodelAugment");
                newModelAugment.mainContent.videoHelper=newHelper;
                [newHelper resetData];
                [newHelper unload];
                [newHelper load:newModelAugment.mainContent.cloudURL playImmediately:NO fromPosition:0.0f];
            }
            NSMutableArray *imageList=[tempAugmentToRender getMediaList];
            int count=imageList.count;
            if(count>0){
                GLuint tex[count];
                for(int i=0;i<count;i++){
                    tex[i]=((MODELMedia*)[imageList objectAtIndex:i]).textureID;
                }
                // glDeleteTextures(count, tex);
                //  }
                
            }
            
            if(newModelAugment.mainContent.mediaType == IMAGE){
                NSLog(@"SINDHU............type IMAGE set %@",newModelAugment.mainContent.cloudURL);
            }
            else if(newModelAugment.mainContent.mediaType == VIDEO){
                NSLog(@"SINDHU............type VIDEO set %@",newModelAugment.mainContent.cloudURL);
            }
            if(tempAugmentToRender.mainContent.mediaType == VIDEO){
                if(tempAugmentToRender.mainContent.videoHelper !=nil){
                    [tempAugmentToRender.mainContent.videoHelper pause];
                    NSLog(@"Paused4") ;
                    
                }
            }
            tempAugmentToRender = newModelAugment;
            if(tempAugmentToRender.mainContent.mediaType == VIDEO){
                if(tempAugmentToRender.mainContent.videoHelper !=nil){
                    if(
                       [tempAugmentToRender.mainContent.videoHelper getStatus]==PLAYING ||
                       [tempAugmentToRender.mainContent.videoHelper getStatus]==READY   ||
                       [tempAugmentToRender.mainContent.videoHelper getStatus]==PAUSED
                       ){
                        [self setShouldAutoPlay:YES];
                        [tempAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
                        currentAugmentToRender = tempAugmentToRender;
                    }
                }
            }else{
                [self setShouldAutoPlay:YES];
                [tempAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
                currentAugmentToRender=tempAugmentToRender;
            }
            
            
            
            if(tempAugmentToRender.canBeRendered)
            {
                // [self updateModelAugment:modelAugmentToRender];
                
            }
            NSLog(@"MainContentURL %@",tempAugmentToRender.mainContent.cloudURL);
        }else
        {
            NSLog(@"ModelAugmebt is nil");
        }
    }else{
        NSLog(@"CRASH");
    }
}

- (void) dealloc{
    [self deleteFramebuffer];
    
    // Tear down context
    if ([EAGLContext currentContext] == context) {
        [EAGLContext setCurrentContext:nil];
    }
    stateChangeListeners = nil;
    
    
    //    for(int i=0;i<3;i++){
    //        elements[i]=nil;
    //    }
    
    tempAugmentToRender.mainContent.videoHelper = nil;
    
}



//---------------------------OPENGL methoads for rendering-----------------------------------------


#pragma mark - UIGLViewProtocol methods

// Draw the current frame using OpenGL. This method is called by Vuforia when it wishes to render the current frame to the screen.
// *** Vuforia will call this method periodically on a background thread ***
- (void)renderFrameVuforia{
    
    
    // test if the layout has changed
    if (self.mDoLayoutSubviews) {
        [self doLayoutSubviews];
        self.mDoLayoutSubviews = NO;
        
    }
    
    [_framebufferLock lock];
    [self setFramebuffer];
    
    // Clear colour and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Begin Vuforia rendering for this frame, retrieving the tracking state
    Vuforia::State state = Vuforia::Renderer::getInstance().begin();
    
    // Render the video background
    Vuforia::Renderer::getInstance().drawVideoBackground();
    
    glEnable(GL_DEPTH_TEST);
    
    // We must detect if background reflection is active and adjust the culling
    // direction.  If the reflection is active, this means the pose matrix has
    // been reflected as well, therefore standard counter clockwise face culling
    // will result in "inside out" models
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    if(Vuforia::Renderer::getInstance().getVideoBackgroundConfig().mReflection == Vuforia::VIDEO_BACKGROUND_REFLECTION_ON) {
        // Front camera
        glFrontFace(GL_CW);
    }
    else {
        // Back camera
        glFrontFace(GL_CCW);
    }
    
    // Get the active trackables
    //    int numActiveTrackables = state.getNumTrackableResults();
    
    // ----- Synchronise data access -----
    [dataLock lock];
    
    
    isActive = NO;// Assume all targets are inactive (used when determining tap locations)
    
    
    // Set the viewport
    glViewport(vapp.viewport.posX, vapp.viewport.posY, vapp.viewport.sizeX, vapp.viewport.sizeY);
    
    
    //------------BODY----------------------
    
    
    
    
    
    
    
    // Did we find any trackables this frame?
    if(state.getNumTrackableResults()>0){
        
        if(_playOnFocus){
            _playOnFocus=NO;
            if(currentAugmentToRender.mainContent.videoHelper!=nil){
                [self setShouldAutoPlay:YES];
            }
            
        }
        
        [self setIsCurrentlyRendering:YES];
        //NSLog(@"Currently playing :::%i",isCurrentlyRendering);
        
        MODELMedia *mainContent = currentAugmentToRender.mainContent;
        
        //        if(mainContent!=nil){
        // Get the trackable
        trackableResult         = state.getTrackableResult(0);
        const Vuforia::ImageTarget  &imageTarget    = (const Vuforia::ImageTarget&) trackableResult->getTrackable();
        
        // we reset this transitional state
        if (self.renderState == RS_OVERLAY) {
            self.renderState = RS_NORMAL;
            
        }
        
        NSString *targetName = [NSString stringWithUTF8String:imageTarget.getName()];
        if(currentAugmentToRender==nil)return;
        mainContent = currentAugmentToRender.mainContent;
        
        modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(trackableResult->getPose());//This matrix is used to calculate the location of tap
        isActive        = YES;// Mark this video (target) as active
        
        
        
        // Get the target size (used to determine if taps are within the target)
        Vuforia::Vec3F size = imageTarget.getSize();
        trackableWidth = size.data[0];
        trackableHeight = size.data[1];
        trackableSize.data[0] = size.data[0];
        trackableSize.data[1] = size.data[1];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            trackableSize.data[0] *= 1.25f;
            trackableSize.data[1] *= 1.25f;
        }
        
        
        // The pose delivers the centre of the target, thus the dimensions
        // go from -width / 2 to width / 2, and -height / 2 to height / 2
        trackableWidth /= 2.0f;
        trackableHeight /= 2.0f;
        if (!self.isViewingTarget )
        {
            [self targetReacquired];
        }
        
        self.isViewingTarget = YES;
        
        //For analytics
        
        uniqueTargetId = [NSString stringWithUTF8String:imageTarget.getUniqueTargetId()];
        [vcMotionPrint pauseVideoPlayerwithTrackableID:uniqueTargetId];

            if([currentTargetID length] == 0) {
            timer=[vcMotionPrint setTimer];
            currentTargetID=uniqueTargetId;
            idleTimer = timer - idleTimer;
            idleTimer/=1000;
            NSLog(@"Idle timer in render::::%ld",idleTimer);
            if(idleTimer<100){
                NSNumber *loadingBarDuration=[NSNumber numberWithLong:idleTimer];
                modelKeys = [vcMotionPrint arKeys];
                [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onNewCampaignScanned:currentTargetID idleTimer:loadingBarDuration];
            }
            loadingDuration = 0;
            
        }else if(![currentTargetID isEqualToString:uniqueTargetId]) {
            
            augmentDuration = [vcMotionPrint setTimer]-timer;
            augmentDuration/=1000;
            NSLog(@"augmentDuration::::%ld",augmentDuration);
            currentTargetID=uniqueTargetId;
            NSString *networkStatus=[self inNetworkConnection];
            NSLog(@"::::::::%@:::::::::",networkStatus);
            currentTargetID=nil;
            NSNumber *duration = [NSNumber numberWithLong:augmentDuration];
            NSNumber *loadingBarDuration=[NSNumber numberWithLong:loadingDuration];
            NSNumber *idleDuration=[NSNumber numberWithLong:idleTimer];
            modelKeys = [vcMotionPrint arKeys];
            if(idleTimer<100){
                [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onNewCampaignScanned:uniqueTargetId idleTimer:idleDuration];
                
            }
            //data push to analytics
            // [[UTILAnalyticsManager sharedInstance] onNewCampaignScanned:currentTargetID idleTimer:idleDuration];
            snapToScreen =0;
            NSNumber *snapduration = [NSNumber numberWithLong:snapToScreen];
            
            [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onCampaignViewedTimeUpdate:uniqueTargetId InteractionDuration:duration LoadingDuration:loadingBarDuration snapToScreenDuration:snapduration networkStatus:networkStatus];
            timer=[vcMotionPrint setTimer];
            loadingDuration=0;
            
        }
        
        
        VideoHelper *videoPlayerHelper=currentAugmentToRender.mainContent.videoHelper;
        [self setLoadingAugmentDimension:trackableWidth trackableHeight:trackableHeight];
        
        if (self.renderState == RS_NORMAL) {
            [self publishStateChange:STATE_3D];
            dispatch_async( dispatch_get_main_queue(), ^{
                [vcMotionPrint scanlineStop];
            });
            modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(trackableResult->getPose());//This matrix is used to calculate the location of tap
            if ( mainContent.mediaType == VIDEO){
                [self loadTexture:mainContent videoHelper:videoPlayerHelper];
                
                
                if(currentAugmentToRender!=tempAugmentToRender){
                    
                    if(tempAugmentToRender.mainContent.videoHelper!=nil && tempAugmentToRender.mainContent.mediaType==VIDEO) {
                        
                        if([tempAugmentToRender.mainContent.videoHelper playbackStarted]){
                            [tempAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
                            currentAugmentToRender=tempAugmentToRender;
                            [self setShouldAutoPlay:YES];
                            videoPlayerHelper=currentAugmentToRender.mainContent.videoHelper;
                            loadingDuration = [vcMotionPrint setTimer]-(vcMotionPrint.loadingVideoDuration);
                            loadingDuration/=1000;
                            NSLog(@"loadingBArDuration::::%ld",loadingDuration);
                            [loadingBarAugment.mainContent.videoHelper unload];
                            NSLog(@"Switched Augment in 2D");
                            
                        }else{
                            [self loadTexture:tempAugmentToRender.mainContent videoHelper:tempAugmentToRender.mainContent.videoHelper];
                            
                        }
                        
                    }
                    
                    
                    
                }
                if ([videoPlayerHelper isRenderable]){
                    
                    
                    [videoPlayerHelper setTextureId:mainContent.textureID];
                    [self renderTexture:mainContent trackablePose:modelViewMatrix];
                    //NSLog(@"texture rendered");
                }
                
            }else{
                [self loadTexture:mainContent videoHelper:nil];
                [self renderTexture:mainContent trackablePose:modelViewMatrix];
            }
            
            
            
            
            NSMutableArray *elements = [currentAugmentToRender elements];
            
            for (int i =0 ; i < elements.count; i++) {
                
                MODELMedia *media = [elements objectAtIndex:i];
                
                modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(trackableResult->getPose());//This matrix is used to calculate the location of tap
                
                //case of elements loading and rendering, to be rendered only after maincontent is rendered
                if (mainContent.mediaType == VIDEO && media.image!=nil) {
                    
                    if ([videoPlayerHelper isRenderable]){
                        
                        [self loadTexture:media videoHelper:nil];
                        [self renderTexture:media trackablePose:modelViewMatrix];
                        
                    }
                } else if (tempAugmentToRender.mainContent.mediaType == IMAGE) {
                    [self loadTexture:media videoHelper:nil];
                    [self renderTexture:media trackablePose:modelViewMatrix];
                }
                
            }}
        else if (self.renderState == RS_TRANSITION_TO_3D) {
            [self publishStateChange:STATE_3D];

            
            if (self.startTransition2Dto3D)
            {
                self.transitionDuration = 0.5f;
                
                //Starts the Transition
                self.transition2Dto3D->startTransition(self.transitionDuration, true, true);
                //Initialize control state variables
                self.startTransition2Dto3D = NO;
            }
            else
            {
                //Checks if the transitions has not finished
                if (!self.reportedFinished2Dto3D)
                {
                    
                    [currentAugmentToRender.mainContent.videoHelper setTextureId:currentAugmentToRender.mainContent.textureID];
                    //Renders the transition
                    self.transition2Dto3D->render(vapp.projectionMatrix, trackableResult->getPose(), trackableSize, currentAugmentToRender.mainContent.textureID, currentAugmentToRender.mainContent);
                    
                    // check if transition is finished
                    if (self.transition2Dto3D->transitionFinished())
                    {
                        //updates current renderState when the transition is finished
                        // to go back to normal rendering
                        self.startTransition2Dto3D = NO;
                        self.renderState = RS_NORMAL;
                        self.isShowing2DOverlay = NO;
                    }
                }
            }
        }
    }else{
        if(isCurrentlyRendering==YES){
            //            dispatch_async( dispatch_get_main_queue(), ^{
            //               [vcMotionPrint scanlineStart];
            //            });
        }
        // _isCurrentlyRendering =NO;
        
        //        if(!([currentTargetID length]==0)){
        //        augmentDuration = [vcMotionPrint setTimer]-timer;
        //        augmentDuration/=1000;
        //            NSLog(@"augmentDuration::::%ld",augmentDuration);
        //            NSString *networkStatus=[self inNetworkConnection];
        //            NSLog(@"::::::::%@:::::::::",networkStatus);
        //            NSNumber *duration = [NSNumber numberWithLong:augmentDuration];
        //            NSNumber *loadingBarDuration=[NSNumber numberWithLong:loadingDuration];
        //
        //            [[UTILAnalyticsManager sharedInstance] onCampaignViewedTimeUpdate:currentTargetID InteractionDuration:duration LoadingDuration:loadingBarDuration networkStatus:networkStatus];
        //            currentTargetID=nil;
        //            loadingDuration=0;
        //
        //        }
        // NSLog(@"Currently playing :::%i",isCurrentlyRendering);
        if (self.renderState == RS_TRANSITION_TO_2D) {
            if (self.startTransition) {
                self.transitionDuration = 0.5f;
                //Starts the Transition
                self.transition3Dto2D->startTransition(_transitionDuration, false, true);
                //Initialize control state variables
                self.startTransition = NO;
                self.reportedFinished = NO;
            } else {
                //Checks if the transitions has not finished
                if (!self.reportedFinished) {
                    [self publishStateChange:STATE_2D];

                    VideoHelper *videoPlayerHelper=currentAugmentToRender.mainContent.videoHelper;
                    MODELMedia *mainContent=currentAugmentToRender.mainContent;
                    //Renders the transition
                    
                    if(currentAugmentToRender!=tempAugmentToRender){
                        
                        if(tempAugmentToRender.mainContent.videoHelper!=nil && tempAugmentToRender.mainContent.mediaType==VIDEO) {
                            
                            if([tempAugmentToRender.mainContent.videoHelper playbackStarted]){
                                [tempAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
                                currentAugmentToRender=tempAugmentToRender;
                                [self setShouldAutoPlay:YES];
                                videoPlayerHelper=currentAugmentToRender.mainContent.videoHelper;
                                [loadingBarAugment.mainContent.videoHelper unload];
                                NSLog(@"Switched Augment");
                                
                            }else{
                                [self loadTexture:tempAugmentToRender.mainContent videoHelper:tempAugmentToRender.mainContent.videoHelper];
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                    //                    if(currentAugmentToRender!=tempAugmentToRender){
                    //                        if(tempAugmentToRender.mainContent.videoHelper!=nil && tempAugmentToRender.mainContent.mediaType==VIDEO){
                    //                            [tempAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
                    //                            currentAugmentToRender = tempAugmentToRender;
                    //                            NSLog(@"SIndhu set");
                    //
                    //                        }else{
                    //                            [self loadTexture:tempAugmentToRender.mainContent videoHelper:tempAugmentToRender.mainContent.videoHelper];
                    //                        }
                    //                    }
                    
                    [self loadTexture:currentAugmentToRender.mainContent videoHelper:currentAugmentToRender.mainContent.videoHelper];
                    MODELMedia *maincontent=currentAugmentToRender.mainContent;
                    if(maincontent.mediaType==VIDEO){
                        CGSize videoSize=[currentAugmentToRender.mainContent.videoHelper getVideoDimension];
                        maincontent.actualWidth=videoSize.width;
                        maincontent.actualHeight=videoSize.height;
                        
                    }
                    if(mainContent.mediaType==VIDEO){
                        if([videoPlayerHelper isRenderable]){
                            NSLog(@"Uniqueee::::%@",uniqueTargetId);

                            [videoPlayerHelper setTextureId:mainContent.textureID];
                            self.transition3Dto2D->render(vapp.projectionMatrix, trackableResult->getPose(), trackableSize, currentAugmentToRender.mainContent.textureID, currentAugmentToRender.mainContent);
                        }else{
                            
                            NSLog(@"Not rendering");
                            //[videoPlayerHelper setTextureId:mainContent.textureID];
                            self.transition3Dto2D->render(vapp.projectionMatrix, trackableResult->getPose(), trackableSize, currentAugmentToRender.mainContent.textureID, currentAugmentToRender.mainContent);
                            
                            
                            
                        }
                    }else{
                        self.transition3Dto2D->render(vapp.projectionMatrix, trackableResult->getPose(), trackableSize, currentAugmentToRender.mainContent.textureID, currentAugmentToRender.mainContent);

                    
                    }
                    // check if transition is finishedpo
                    if (self.transition3Dto2D->transitionFinished() && !self.reportedFinished) {
                        
                        //[self endTransitionOnTargetLost];
                    }
                }
                
            }
        } if (self.renderState == RS_OVERLAY) {
            // if the overlay view was displayed while no target was found, we
            // need to trigger the event so that the targets shows up
            self.renderState = RS_NORMAL;
            self.isShowing2DOverlay = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kTargetLost" object:nil userInfo:nil];
        }
        
        if (_isViewingTarget) { // This means there was a target but we can't find it anymore
            self.isViewingTarget = NO;
            
            // This needs to be called on main thread to make sure the thread doesn't die before the timer is called
            dispatch_async(dispatch_get_main_queue(), ^{
                [self targetLost];
            });
        }
    }
    
    // --- INFORMATION ---
    // One could pause automatic playback of a video at this point.  Simply call
    // the pause method of the VideoHelper object without setting the timer (as below).
    // --- END INFORMATION ---
    
    // If a video is playing on texture and we have lost tracking, create a
    // timer on the main thread that will pause video playback after TRACKING_LOST_TIMEOUT seconds
    
    if (trackingLostTimer ==nil && !isActive && [tempAugmentToRender.mainContent.videoHelper getStatus]== PLAYING) {
        [self performSelectorOnMainThread:@selector(createTrackingLostTimer) withObject:nil waitUntilDone:YES];
    }
    
    [dataLock unlock];
    // ----- End synchronise data access -----
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    
    Vuforia::Renderer::getInstance().end();
    [self presentFramebuffer];
}

//    }else {   // There is no trackable target
//        if (self.renderState == RS_TRANSITION_TO_2D) {
//            if (self.startTransition) {
//                //Starts the Transition
//                self.transition3Dto2D->startTransition(_transitionDuration, false, true);
//                //Initialize control state variables
//                self.startTransition = NO;
//                self.reportedFinished = NO;
//            } else {
//                //Checks if the transitions has not finished
//                if (!self.reportedFinished) {
//
//                    //Renders the transition
//                    self.transition3Dto2D->render(vapp.projectionMatrix, trackableResult->getPose(), trackableSize, modelAugmentToRender.mainContent.textureID, modelAugmentToRender.mainContent);
//
//                    // check if transition is finished
//                    if (self.transition3Dto2D->transitionFinished() && !self.reportedFinished) {
//                        [self endTransitionOnTargetLost];
//                    }
//                }
//            }
//        } if (self.renderState == RS_OVERLAY) {
//            // if the overlay view was displayed while no target was found, we
//            // need to trigger the event so that the targets shows up
//            self.renderState = RS_NORMAL;
//            self.isShowing2DOverlay = YES;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"kTargetLost" object:nil userInfo:nil];
//        }
//
//        if (_isViewingTarget) { // This means there was a target but we can't find it anymore
//            self.isViewingTarget = NO;
//
//            // This needs to be called on main thread to make sure the thread doesn't die before the timer is called
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self targetLost];
//            });
//        }
//    }

// --- INFORMATION ---
// One could pause automatic playback of a video at this point.  Simply call
// the pause method of the VideoHelper object without setting the timer (as below).
// --- END INFORMATION ---

// If a video is playing on texture and we have lost tracking, create a
// timer on the main thread that will pause video playback after TRACKING_LOST_TIMEOUT seconds

//    if (trackingLostTimer ==nil && !isActive && [tempAugmentToRender.mainContent.videoHelper getStatus]== PLAYING) {
//        [self performSelectorOnMainThread:@selector(createTrackingLostTimer) withObject:nil waitUntilDone:YES];
//    }
//
//    [dataLock unlock];
//    // ----- End synchronise data access -----
//
//    glDisable(GL_DEPTH_TEST);
//    glDisable(GL_CULL_FACE);
//
//    Vuforia::Renderer::getInstance().end();
//    [self presentFramebuffer];
//}

- (void) loadTexture:(MODELMedia*)media videoHelper:(VideoHelper*) videoPlayerHelper {
    if(media!=nil){
        Texture *texture = media.texture;
        if (media.mediaType == IMAGE) {
            
            if (texture == nil )
            {
                GLuint textureID = [self createOpenGLTexture];
                
                Texture *texture = [[Texture alloc] initWithUIImage:media.image];
                [media setTexture:texture];
                [media setTextureID:textureID];
                [media setRendered:YES];
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, [texture width], [texture height], 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)[texture pngData]);
            }
            
        } else if (media.mediaType == VIDEO) {
            
            
            
            if (media.textureID == 0) {
                
                GLuint textureID = [self createOpenGLTexture];
                NSLog(@"Texture created");
                [media setTextureID:textureID];
            }else if (![[videoPlayerHelper getUrl]  isEqualToString:media.cloudURL]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [videoPlayerHelper setShouldAutoPlay:YES];
                    [self setShouldAutoPlay:YES];
                    [self prepare:media.cloudURL];
                });
                
            }else if (videoPlayerHelper != nil && [videoPlayerHelper isPlayableOnTexture]) {
                
                switch ([videoPlayerHelper getStatus]) {
                    case PLAYING:
                        if([videoPlayerHelper playbackStarted])[videoPlayerHelper updateVideoData:media.cloudURL];
                        //NSLog(@"video data updated");
                        //setVideoDimensions();//TODO wht is relevant method in iOS
                        break;
                    case REACHED_END:
                        
                    case READY:
                        if(_shouldAutoPlay){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [videoPlayerHelper play:NO fromPosition:VIDEO_PLAYBACK_CURRENT_POSITION];
                               // [tempAugmentToRender.mainContent.videoHelper pause];
                                NSLog(@"Play4");
                                NSLog(@"rendeing::::::%d",isCurrentlyRendering);
                                
                                
                            });
                        }
                        break;
                        
                    case PAUSED:
                        
                        if( _shouldAutoPlay){
                            //if(vcMotionPrint.playResumed){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //   CMTime seekTime = [_objFeedBlowUpVC getCurrentSeekTime];
                                
                                [videoPlayerHelper play:NO fromPosition:VIDEO_PLAYBACK_CURRENT_POSITION];
                                NSLog(@"Play5");
                                
                            });
                        }
                        //  }
                        
                        break;
                        
                }
                
            }
            
            
        }
    }}

- (GLuint) createOpenGLTexture{
    
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    return textureID;
}

- (void) renderTexture:(MODELMedia*)mediaToLoad trackablePose:(Vuforia::Matrix44F) modelViewMatrix{
    //  _isElementInAction=NO;
    
    Texture             *texture        = mediaToLoad.texture;
    Vuforia::Matrix44F  projMatrix      = vapp.projectionMatrix;
    GLuint              textureID       = [mediaToLoad textureID];
    
    
    
    // Convert trackable pose to matrix for use with OpenGL
    
    Vuforia::Matrix44F modelViewProjectionButton;
    
    float traslateX = trackableWidth * mediaToLoad.x;
    float traslateY = trackableHeight * mediaToLoad.y;
    float traslateZ = mediaToLoad.z;
    SampleApplicationUtils::translatePoseMatrix(traslateX, traslateY, traslateZ, &modelViewMatrix.data[0]);
    
    
    float scaleWidth    = trackableWidth * mediaToLoad.width;
    float scaleHeight   = trackableHeight * mediaToLoad.height;
    
    if(mediaToLoad.mediaType == VIDEO && ![currentAugmentToRender.mainContent.videoHelper isScalable]){
        scaleWidth  = 0;
        scaleHeight = 0;
    }
    
    SampleApplicationUtils::scalePoseMatrix(scaleWidth, scaleHeight, trackableWidth, &modelViewMatrix.data[0]);
    SampleApplicationUtils::multiplyMatrix(projMatrix.data, &modelViewMatrix.data[0] , &modelViewProjectionButton.data[0]);
    
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //    glEnable    (GL_COLOR_MATERIAL);
    GLuint shaderIdToUse = (mediaToLoad == loadingBarAugment.mainContent && mediaToLoad.mediaType==VIDEO )?loadingBarShaderID:shaderID;
    glUseProgram(shaderIdToUse);
    glVertexAttribPointer(vertexHandle, 3, GL_FLOAT, GL_FALSE, 0, quadVertices);
    glVertexAttribPointer(normalHandle, 3, GL_FLOAT, GL_FALSE, 0, quadNormals);
    glVertexAttribPointer(textureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0, (mediaToLoad.mediaType == IMAGE)?quadTexCoords : videoQuadTextureCoords);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    
    glEnableVertexAttribArray(vertexHandle);
    glEnableVertexAttribArray(normalHandle);
    glEnableVertexAttribArray(textureCoordHandle);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniformMatrix4fv(mvpMatrixHandle, 1, GL_FALSE, (GLfloat*)&modelViewProjectionButton.data[0] );
    glDrawElements(GL_TRIANGLES, kNumQuadIndices, GL_UNSIGNED_SHORT, quadIndices);
    
    
    glDisableVertexAttribArray(vertexHandle);
    glDisableVertexAttribArray(normalHandle);
    glDisableVertexAttribArray(textureCoordHandle);
    glUseProgram(0);
    glDisable(GL_BLEND);
    glDepthFunc(GL_LESS);
    
}



// -----------------------VIDEO PLAYER METHODS---------------------------



- (void) preparePlayers {
    //[self prepare:currentAugmentToRender.mainContent.cloudURL];
    //[self prepare];
}

- (void) dismissPlayers {
    [self dismiss];
}

- (void) prepare :(NSString*)url{
    // For each target, create a VideoHelper object and zero the
    // target dimensions
    // For each target, create a VideoHelper object and zero the
    // target dimensions
    
    
    trackableWidth = 0.0f;
    trackableHeight = 0.0f;
    
    // [videoPlayerHelper setOverLayController:_objFeedBlowUpVC];
    
    
    // Start video playback from the current position (the beginning) on the
    // first run of the app
    //for (int i = 0; i < kNumVideoTargets; ++i) {
    
    // }
    
    // For each video-augmented target
    for (int i = 0; i < kNumVideoTargets; ++i) {
        // Load a local file for playback and resume playback if video was
        // playing when the app went into the background
        VideoHelper* player = currentAugmentToRender.mainContent.videoHelper;
        // NSString    *filename=@"portrait.mp4";
        //        if(trackableWidth>trackableHeight){
        //            NSLog(@"SINDHU landscape");
        //            if (NO == [player load:@"lanscape.m4v.mov" playImmediately:NO fromPosition:videoPlaybackTime]){
        //                NSLog(@"Failed to load media");
        //
        //            }}else{
        //                NSLog(@"SINDHU portrait");
        //
        if (NO == [player load:currentAugmentToRender.mainContent.cloudURL playImmediately:NO fromPosition:VIDEO_PLAYBACK_CURRENT_POSITION]){
            NSLog(@"Failed to load media");
            
            //            }
        }
    }
    
    //
    //        switch (i) {
    //            case 0:
    //                filename = @"VuforiaSizzleReel_1.mp4";
    //                break;
    //            default:
    //                filename = @"VuforiaSizzleReel_2.mp4";
    //                break;
    //        }
    
    
    
    
}

- (BOOL) stop{
    return [tempAugmentToRender.mainContent.videoHelper stop];
}

- (void) dismiss {
    
    // [modelAugmentToRender.mainContent.videoHelper unload];
    // modelAugmentToRender.mainContent.videoHelper = nil;
    
}



//----------------------------------OnTouch methods--------------------------------------------
#pragma mark - User interaction

- (bool) handleTouchPoint:(CGPoint) point {
    NSLog(@"%d:::::::::::",self.renderState);
    modelKeys = [vcMotionPrint arKeys];
    
    if (self.renderState == RS_NORMAL && isCurrentlyRendering) {
        
        MODELMedia *touchedMedia = [self isAnyAugmentTouched:point.x touchY:point.y];
        
        
        if(touchedMedia == currentAugmentToRender.mainContent ){
            
            if(touchedMedia.mediaType == VIDEO){
                [self onMainContentVideoTouched];
                NSLog(@"NORMAL RENDERING");
                [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onCampaignPlaybackToggled:currentTargetID action:state];
            }
            return YES;
            
            
        }else{
            
            
            MODELAction *action = touchedMedia.modelAction;
            if(action!=nil){
                [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onCampaignElementTouched:touchedMedia.mediaId];
                
                [self applyAction:action];
                elementTouched = YES ;
                NSLog(@"Element touched");
                
            }
            
        }
        
        
    }else if (isCurrentlyRendering && (self.renderState == RS_TRANSITION_TO_2D) ){
        float offTargetWidth = self.transition3Dto2D->getOffTargetWidth();
        float offTargetHeight = self.transition3Dto2D->getOffTargetHeight();
        float y2,y1,x1,x2;
        float touchX = point.x*[UIScreen mainScreen].nativeScale;
        float touchY = point.y*[UIScreen mainScreen].nativeScale;
        CGSize screenSize =[vapp getCurrentARViewBoundsSize];
        
        
        x1 = screenSize.width/2.0f - (offTargetWidth);
        y1 = screenSize.height/2.0f - (offTargetHeight);
        
        x2 = x1 + (2*offTargetWidth);
        if ([VuforiaHelper isRetinaDevice] && [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            y2 = y1 +128.0f+ (2*offTargetHeight);
            y1 = (screenSize.height/2.0f) - (offTargetHeight+128.0f);
            
        }else{
            y2 = y1 + (2*offTargetHeight);
        }
        
        if ((touchX <= x2) && (touchX >= x1) && (touchY <= y2)&& (touchY>= y1)){
            
            NSLog(@">>>>>>>>YES it is inside off target main content");
            [self togglePlayBack:YES];
            NSLog(@"SSNAP TO SCREEN");
            [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onCampaignPlaybackToggled:currentTargetID action:state];
            
        }else{
            NSLog(@">>>>>>>>NO it is not inside off target main content");
        }
        
    }
    return NO;
}




// -----------------------ELEMENTS ACTION METHODS---------------------------


- (MODELMedia*)isAnyAugmentTouched:(float)touchX touchY:(float)touchY{
    
    if(tempAugmentToRender == nil) return nil;
    touchX *= [UIScreen mainScreen].nativeScale;
    touchY *= [UIScreen mainScreen].nativeScale;
    CGSize screenSize =[vapp getCurrentARViewBoundsSize];
    
    
    Vuforia::Matrix44F projectionMatrix = [vapp projectionMatrix];
    Vuforia::Matrix44F inverseProjMatrix = SampleMath::Matrix44FInverse(projectionMatrix);
    
    Vuforia::Vec3F intersection, lineStart, lineEnd;
    SampleMath::projectScreenPointToPlane(inverseProjMatrix, modelViewMatrix, screenSize.width, screenSize.height,
                                          Vuforia::Vec2F(touchX, touchY), Vuforia::Vec3F(0, 0, 0), Vuforia::Vec3F(0, 0, 1), intersection, lineStart, lineEnd);
    
    NSMutableArray *mediaList = [currentAugmentToRender getMediaList];
    NSUInteger mediaCount=currentAugmentToRender.elements.count;
    MODELMedia *touchedMedia = nil;
    for (int i = 0; i <=mediaCount; i++) {
        
        
        MODELMedia *elementMedia = [mediaList objectAtIndex:i];
        float elementX  =  elementMedia.x*trackableWidth;
        float elementY  =  elementMedia.y*trackableHeight;
        float elementZ  =  elementMedia.z;
        float scaleX    =  elementMedia.width;
        float scaleY    =  elementMedia.height ;
        float touchX    =  intersection.data[0];
        float touchY    =  intersection.data[1];
        float x1        =  (elementX-((trackableWidth*scaleX)));
        float x2        =  (elementX+((trackableWidth*scaleX)));
        float y1        =  (elementY-((trackableHeight*scaleY)));
        float y2        =  (elementY+((trackableHeight*scaleY)));
        float padding   =   5.0f;
        NSLog(@"----------");
        
        //NSLog(@"%i::: X__%f>%f>%f ::: Y___%f >%f >%f",i,x1,touchX,x2,y1,touchY,y2);
        
        
        NSLog(@"----------");
        if ((touchX <= (x2+padding)) && (touchX >= (x1-padding)) && (touchY <= (y2+padding))&& (touchY>= (y1-padding))){
            
            if (touchedMedia == nil || elementZ > touchedMedia.z){
                touchedMedia = elementMedia;
            }
        }
    }
    
    
    
    
    if (touchedMedia != nil){
        
        NSLog(@">>>>>>>>YES it is inside element %@",touchedMedia.cloudURL);
    }else{
        NSLog(@">>>>>>>>NO element matched touch");
    }
    
    return touchedMedia;
}

- (void)applyAction:(MODELAction*)action{
    
    switch ([action getActionType]) {
        case VISIT_URL:
            [self redirectToURL:action.data1];
            break;
        case CALL:
            [self makeCall:action.data1];
            break;
        case MAP:
            [self openMaps:action.data1];
            break;
        case WHATSAPP:
            [self sendWhtsApp:action.data1 message:action.data2];
            break;
            
        case SMS:
            [self sendSMS:action.data1 message:action.data2];
            break;
        case TWEET:
            [self tweet:action.data1];
            break;
        case FB_POST:
            [self postOnFB:action.data1];;
            break;
    }
    
    
}

- (void)redirectToURL:(NSString*)url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)makeCall:(NSString*)numberToCall{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:numberToCall];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}

- (void)openMaps:(NSString*)latLong{
    NSString *nativeMapScheme = @"maps.google.com";
    NSString* url = [NSString stringWithFormat:@"http://%@/maps?q=%@", nativeMapScheme,latLong];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)sendWhtsApp:(NSString*)callNumber message:(NSString*)message{
    NSString * msg = @"HEllo";
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
};

- (void)tweet:(NSString*)message{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:message];
        [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
            }
            [vcMotionPrint dismissViewControllerAnimated:YES completion:nil];
            
        }];
        [vcMotionPrint presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Twitter not installed." message:@"Your device has no Twitter installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    
    
};

- (void)postOnFB:(NSString*)message{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText:message];
        [facebookSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
            }
            [vcMotionPrint dismissViewControllerAnimated:YES completion:nil];
            
        }];
        [vcMotionPrint presentViewController:facebookSheet animated:YES completion:nil];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Facebook not installed." message:@"Your device has no Facebook installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (void)sendSMS:(NSString*)phoneNumber message:(NSString*)message{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body =message;
        controller.recipients = [NSArray arrayWithObjects:phoneNumber, nil];
        controller.messageComposeDelegate = self;
        
        [vcMotionPrint presentViewController:controller animated:YES completion:nil];
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [vcMotionPrint dismissViewControllerAnimated:YES completion:nil];
    
    
    
}





//------------------------------------------------------------------------------
#pragma mark - OpenGL ES management

- (void)initShaders{//TODO In Android shaderid vertex handle etc are being created for each media, whereas iOS its being created only once
    
    
    loadingBarShaderID   =   [SampleApplicationShaderUtils createProgramWithVertexShaderFileName:@"Simple.vertsh" fragmentShaderFileName:@"Simple.fragsh"];
    
    shaderID             =   [SampleApplicationShaderUtils createProgramWithVertexShaderFileName:@"Simple.vertsh" fragmentShaderFileName:@"SimpleImg.fragsh"];
    
    
    if (loadingBarShaderID> 0) {
        //glClearColor(0.0f, 0.0f, 0.0f, Vuforia::requiresAlpha() ? 0.0f : 1.0f);
        vertexHandle = glGetAttribLocation(loadingBarShaderID, "vertexPosition");
        normalHandle = glGetAttribLocation(loadingBarShaderID, "vertexNormal");
        textureCoordHandle = glGetAttribLocation(loadingBarShaderID, "vertexTexCoord");
        mvpMatrixHandle = glGetUniformLocation(loadingBarShaderID, "modelViewProjectionMatrix");
        texSampler2DHandle  = glGetUniformLocation(loadingBarShaderID,"texSampler2D");
    }
    else {
        NSLog(@"Could not initialise augmentation shader");
    }
    
    if (0 < shaderID) {
        //  glClearColor(0.0f, 0.0f, 0.0f,1.0f);
        vertexHandle = glGetAttribLocation(shaderID, "vertexPosition");
        normalHandle = glGetAttribLocation(shaderID, "vertexNormal");
        textureCoordHandle = glGetAttribLocation(shaderID, "vertexTexCoord");
        mvpMatrixHandle = glGetUniformLocation(shaderID, "modelViewProjectionMatrix");
        texSampler2DHandle  = glGetUniformLocation(shaderID,"texSampler2D");
    }
    else {
        NSLog(@"Could not initialise augmentation Img shader");
    }
    
}


- (void)createFramebuffer{
    if (context) {
        // Create default framebuffer object
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        // Create colour renderbuffer and allocate backing store
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        // Allocate the renderbuffer's storage (shared with the drawable object)
        [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
        GLint framebufferWidth;
        GLint framebufferHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &framebufferWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);
        
        // Create the depth render buffer and allocate storage
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, framebufferWidth, framebufferHeight);
        
        // Attach colour and depth render buffers to the frame buffer
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        // Leave the colour render buffer bound so future rendering operations will act on it
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    }
}


- (void)deleteFramebuffer{
    if (context) {
        [EAGLContext setCurrentContext:context];
        
        if (defaultFramebuffer) {
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        
        if (colorRenderbuffer) {
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            colorRenderbuffer = 0;
        }
        
        if (depthRenderbuffer) {
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
    }
}


- (void)setFramebuffer{
    // The EAGLContext must be set for each thread that wishes to use it.  Set
    // it the first time this method is called (on the render thread)
    if (context != [EAGLContext currentContext]) {
        [EAGLContext setCurrentContext:context];
    }
    
    if (!defaultFramebuffer) {
        // Perform on the main thread to ensure safe memory allocation for the
        // shared buffer.  Block until the operation is complete to prevent
        // simultaneous access to the OpenGL context
        [self performSelectorOnMainThread:@selector(createFramebuffer) withObject:self waitUntilDone:YES];
    }
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
}


- (BOOL)presentFramebuffer{
    // setFramebuffer must have been called before presentFramebuffer, therefore
    // we know the context is valid and has been set for this (render) thread
    
    // Bind the colour render buffer and present it
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    return [context presentRenderbuffer:GL_RENDERBUFFER];
}


// -----------------------OPEN GL MGMT CUSTOM METHODS---------------------------



- (void) finishOpenGLESCommands{
    // Called in response to applicationWillResignActive.  The render loop has
    // been stopped, so we now make sure all OpenGL ES commands complete before
    // we (potentially) go into the background
    if (context) {
        [EAGLContext setCurrentContext:context];
        glFinish();
    }
}


- (void) freeOpenGLESResources{
    // Called in response to applicationDidEnterBackground.  Free easily
    // recreated OpenGL ES resources
    [self deleteFramebuffer];
    glFinish();
}


// -----------------------TRACKING TIMER METHODS---------------------------


// Create the tracking lost timer
- (void)createTrackingLostTimer{
    trackingLostTimer = [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(trackingLostTimerFired:) userInfo:nil repeats:NO];
}


// Terminate the tracking lost timer
- (void)terminateTrackingLostTimer{
    [trackingLostTimer invalidate];
    trackingLostTimer = nil;
}


// Tracking lost timer fired, pause video playback
- (void)trackingLostTimerFired:(NSTimer*)timer{
    // Tracking has been lost for TRACKING_LOST_TIMEOUT seconds, pause playback
    // (we can safely do this on all our VideoHelpers objects)
    
    //[self setShouldAutoPlay:YES];
    //  [currentAugmentToRender.mainContent.videoHelper pause];
    //NSLog(@"Paused6") ;
    vcMotionPrint.currentTimeMillis=[vcMotionPrint setTimer];
    idleTimer=[vcMotionPrint setTimer];
    trackingLostTimer = nil;
    
}



// -----------------------PUBLIC METHODS---------------------------

- (void) setLoadingBarAugment:(MODELAugment*)givenLoadingBarAugment{
    loadingBarAugment = givenLoadingBarAugment;
    currentAugmentToRender=givenLoadingBarAugment;
    loadingBarAugment.mainContent.shaderID=loadingBarShaderID;
    currentAugmentToRender.mainContent.shaderID=loadingBarShaderID;
}

- (void) resetLoadingAugment{
    if(currentAugmentToRender!=loadingBarAugment){
        if(currentAugmentToRender.mainContent.mediaType==VIDEO){
            if(currentAugmentToRender.mainContent.videoHelper!=nil){
                [currentAugmentToRender.mainContent.videoHelper pause];
                NSLog(@"PAuse10");
            }
        }
    }
    tempAugmentToRender = nil;
    currentAugmentToRender = loadingBarAugment;
    [loadingBarAugment.mainContent.videoHelper unload];
    
    
}


- (BOOL)isCurrentlyRendering{
    return isCurrentlyRendering;
}


-(void)setLoadingAugmentDimension:(float)trackableWidth trackableHeight:(float)trackableHeight{
    
    if((trackableWidth>=trackableHeight)){
        
        loadingBarAugment.mainContent.height=1.0f;
        loadingBarAugment.mainContent.width=trackableHeight/trackableWidth;
        
        
    }else{
        loadingBarAugment.mainContent.width=1.0f;
        loadingBarAugment.mainContent.height=trackableWidth/trackableHeight;
        
    }
}


- (void)setShouldAutoPlay:(BOOL)shouldAutoPlay{
    _shouldAutoPlay = shouldAutoPlay;
    NSLog(@"AUTOPLAY::::%d",shouldAutoPlay);
    [currentAugmentToRender.mainContent.videoHelper setShouldAutoPlay:shouldAutoPlay];
}



-(void)onMainContentVideoTouched{
    // Get the state of the video player for the target the user touched
    MEDIA_STATE mediaState = [currentAugmentToRender.mainContent.videoHelper getStatus];
    
    // If any on-texture video is playing, pause it
    
    if (mediaState == PLAYING ) {
        
        [self setShouldAutoPlay:NO];
        [currentAugmentToRender.mainContent.videoHelper setShouldAutoPlay:NO];
        [currentAugmentToRender.mainContent.videoHelper pause];
        NSLog(@"Paused5") ;
        state = @"evtprop_value_paused";
        
    }else if (mediaState == PAUSED || mediaState == READY){
        
        NSLog(@"MediaState::::::%d",mediaState);
        [currentAugmentToRender.mainContent.videoHelper play:NO fromPosition:VIDEO_PLAYBACK_CURRENT_POSITION];
        NSLog(@"Play6");
        
        [currentAugmentToRender.mainContent.videoHelper setShouldAutoPlay:YES];
        [self setShouldAutoPlay:YES];
        state = @"evtprop_value_played";
        
    }
    
    
}




- (void)setPlayOnFocus:(BOOL)playOnFocus{
    _playOnFocus = playOnFocus;
}

- (void)pauseAugment{
    //    if(currentAugmentToRender.mainContent.videoHelper!=nil && currentAugmentToRender.mainContent.mediaType==VIDEO)
    //        [currentAugmentToRender.mainContent.videoHelper pause];
    if([currentAugmentToRender.mainContent.videoHelper getStatus]==PLAYING){
        [self setShouldAutoPlay:NO];
        [currentAugmentToRender.mainContent.videoHelper pause];
      
}
    
}
-(void)stopPlayback{
    if([currentAugmentToRender.mainContent.videoHelper getStatus]==PLAYING){
        [currentAugmentToRender.mainContent.videoHelper pause];
        //[currentAugmentToRender.mainContent.videoHelper resetData];
        
    }
}
-(NSString*)inNetworkConnection{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NSString *networkStatus;
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        NSLog(@"Not in network connection");
        
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        networkStatus=@"evtprop_value_wifi";
        
    }
    else if (status == ReachableViaWWAN)
    {
        
        networkStatus=@"evtprop_value_cellular";
        
        NSLog(@" in 3G connection");
        
        //3G
    }
    return networkStatus;
    
}

- (void)setTransitionToNormal{
    self.renderState = RS_NORMAL;
    [self setShouldAutoPlay:YES];
    [currentAugmentToRender.mainContent.videoHelper pause];
   // [self togglePlayBack: NO];
}

- (void)togglePlayBack:(BOOL)userRequested{
    
    if(currentAugmentToRender.mainContent.mediaType != VIDEO) return;
    
    // Get the state of the video player for the target the user touched
    MEDIA_STATE mediaState = [currentAugmentToRender.mainContent.videoHelper getStatus];
    
    // If any on-texture video is playing, pause it
    
    if (mediaState == PLAYING ) {
        
        if(userRequested)        [self setShouldAutoPlay:NO];
        [currentAugmentToRender.mainContent.videoHelper pause];
        state = @"evtprop_value_paused";
        NSLog(@"PAUSED");

        
    }else if (mediaState == PAUSED || mediaState == READY){
        [currentAugmentToRender.mainContent.videoHelper play:NO fromPosition:VIDEO_PLAYBACK_CURRENT_POSITION];
        if(userRequested)        [self setShouldAutoPlay:YES];
        state = @"evtprop_value_played";
        NSLog(@"PLAYING");

    }
}

- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    NSLog(@"Registered state listener %@",stateChangeListener);
    if(stateChangeListeners==nil){
        stateChangeListeners = [[NSMutableArray alloc]init];
    }
    [stateChangeListeners addObject:stateChangeListener];
    
}
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    NSLog(@"Removed state listener %@",stateChangeListener);
    if(stateChangeListeners!=nil)
        [stateChangeListeners removeObject:stateChangeListener];
}
-(void)publishStateChange:(int)currentState{
    for(id stateChangeListener in stateChangeListeners){
        NSLog(@"Current RENDERstate by listener::%d",currentState);
        [stateChangeListener onStateChanged:currentState];
    }
}

-(void)sendAnalytics{
    if(!([currentTargetID length]==0)){
        augmentDuration = [vcMotionPrint setTimer]-timer;
        long snapDuration =[vcMotionPrint setTimer]-snapToScreen;
        augmentDuration/=1000;
        snapDuration/=1000;
        NSLog(@"After SnapToScreen::::%ld",augmentDuration);
        NSString *networkStatus=[self inNetworkConnection];
        NSLog(@"::::::::%@:::::::::",networkStatus);
        NSNumber *duration = [NSNumber numberWithLong:augmentDuration];
        NSNumber *loadingBarDuration=[NSNumber numberWithLong:loadingDuration];
        NSNumber *snapScreenDuration=[NSNumber numberWithLong:snapDuration];
        
        modelKeys = [vcMotionPrint arKeys];
        [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onCampaignViewedTimeUpdate:currentTargetID InteractionDuration:duration LoadingDuration:loadingBarDuration snapToScreenDuration:snapScreenDuration networkStatus:networkStatus];
        currentTargetID=nil;
        NSLog(@"HITTTT");
        //loadingDuration=0;
        
    }
    [self setIsCurrentlyRendering:NO];
    
    
}
-(void)setIsCurrentlyRendering:(BOOL)isRendering{
    isCurrentlyRendering = isRendering;
}

@end
