//
//  MODELResultSet.h
//  VideoPlayback
//
//  Created by wowsome on 13/07/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef MODELResultSet_h
#define MODELResultSet_h


#endif /* MODELResultSet_h */


@interface MODELResultSet : NSObject

@property(nonatomic, retain) NSString *error;
@property(nonatomic, retain) NSString *message;
@property(nonatomic, retain) NSObject *data;

- (id) init;
- (id) initWithError:(NSString*) error messages:(NSString*)message;
- (NSString*) getMessage;
- (NSString*) getDoubleLineUIMessage;
- (NSString*) getSingleLineUIMessage;
- (BOOL) hasError;
- (BOOL) hasNoError;
@end