//
//  MODELMedia.m
//  Books
//
//  Created by wowdev on 10/05/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MODELMedia.h"


@implementation MODELMedia
@synthesize mediaType, mediaId, localURL, cloudURL, width, height, texture, image,x,y,z,scale,modelAction, rendered,  textureID,videoFrame,videoHelper,highResolutionImage;
-(void)releaseData{
    mediaType=nil;
    mediaType=nil;
    mediaId=nil;
    localURL=nil;
    cloudURL=nil;
    width=0.0f;
    height=0.0f;
    texture=nil;
    image=nil;
    x=0.0f;
    y=0.0f;
    z=0.0f;
    scale=0.0f;
    modelAction=nil;
    rendered=nil;
    textureID=nil;
    videoFrame=nil;
    highResolutionImage=nil;
    if(videoHelper!=nil){
        [videoHelper stop];
        [videoHelper unload];
        videoHelper=nil;
        
    }
    
}
@end
