//
//  StateChangeListener.h
//  WOWSOMEAR
//
//  Created by Sikandar Khan on 19/10/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#ifndef StateChangeListener_h
#define StateChangeListener_h


#endif /* StateChangeListener_h */


@protocol StateChangeListener <NSObject>
typedef enum {
    STATE_3D = 0,
    STATE_2D
}RenderState;


@property (nonatomic, assign) RenderState states;
@required
-(void) onStateChanged:(int)state;
@end
