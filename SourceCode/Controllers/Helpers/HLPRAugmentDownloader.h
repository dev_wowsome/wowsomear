//
//  HLPRImageDownloader.h
//  VideoPlayback
//
//  Created by wowdev on 30/06/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef HLPRImageDownloader_h
#define HLPRImageDownloader_h


#endif /* HLPRImageDownloader_h */

#import "MODELAugment.h"
#import "MODELKeys.h"


@interface HLPRAugmentDownloader : NSObject

- (id)initWithAugment:(MODELAugment*)augment modelKeys:(MODELKeys*)modelKeys successBlock:(void (^)(MODELAugment *modelAugment))success failBlock:(void(^)(MODELAugment *modelAugment))fail;

- (void) startDownload;
@end
