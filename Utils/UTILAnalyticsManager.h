//
//  UTILAnalyticsManager.h
//  WOWSOME
//
//  Created by Sikandar Khan on 19/09/16.
//  Copyright © 2016 Qualcomm. All rights reserved.
//

#ifndef UTILAnalyticsManager_h
#define UTILAnalyticsManager_h


#endif /* UTILAnalyticsManager_h */
@interface UTILAnalyticsManager : NSObject {
    
}
-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender;
-(void)onCampaignElementTouched:(NSString*)elementId;
-(void)onARLaunched:(NSNumber*)InitDuration;
-(void)onCampaignPlaybackToggled:(NSString*)campaignId action:(NSString*)action;

-(void)onNewCampaignScanned:(NSString*)campaignId idleTimer:(NSNumber*)idleTime;
-(void)onCampaignViewedTimeUpdate:(NSString*)campaignId InteractionDuration:(NSNumber*)InteractionDuration LoadingDuration:(NSNumber*)LoadingDuration snapToScreenDuration:(NSNumber*)snapToScreenDuration networkStatus:(NSString*)networkStatus;




+ (id)sharedInstancewithKey:(NSString*)flurryKey ;
+(id)startSessionWithAgencyId:(NSString*)givenAgencyId agencyName:(NSString*)givenAgencyName flurryKey:(NSString*)flurryKey;

@end
