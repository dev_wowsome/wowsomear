//
//  WOWSOMEManager.h
//  WOWSOMEAR
//
//  Created by wowsome on 14/06/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#ifndef WOWSOMEManager_h
#define WOWSOMEManager_h


#endif /* WOWSOMEManager_h */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WOWSOMEFactoryDelegate.h"
#import "StateChangeListener.h"

@class VCMotionPrint;

@interface LibWOWSOME : NSObject<WOWSOMEFactoryDelegate ,StateChangeListener>

+ (id<WOWSOMEFactoryDelegate>) getInstance:(NSString*)agencyId;



@end
