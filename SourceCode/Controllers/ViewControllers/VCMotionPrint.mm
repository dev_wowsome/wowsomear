/*===============================================================================
 Copyright (c) 2016 PTC Inc. All Rights Reserved.
 
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===================================================================foria ============*/

#import "VCMotionPrint.h"
#import <Vuforia/Vuforia.h>
#import <Vuforia/DataSet.h>
#import <Vuforia/Tool.h>
#import <Vuforia/TrackerManager.h>
#import <Vuforia/ObjectTracker.h>
#import <Vuforia/Trackable.h>
#import <Vuforia/ImageTarget.h>
#import <Vuforia/TargetFinder.h>
#import <Vuforia/CameraDevice.h>
#import "ViewRender.h"
#import "Texture.h"
#import "HLPRAugmentDownloader.h"
#import "DFFeeds.h"
#import "AsyncImageDownloader.h"
#import <Vuforia/TrackableResult.h>
#import "UIView+Toast.h"
#import "WowsomePrefixHeader.pch"
#import "DotActivityIndicatorParms.h"
#import "DotActivityIndicatorView.h"
#import "UTILAnalyticsManager.h"
#import "MODELKeys.h"

@class ViewRender;

@interface VCMotionPrint (){
    int invalidScanCount;
    long lastInvalidToastShownAt;
    BOOL isViewControllerInForeground;
    BOOL isWifiEnabled;
    UIAlertView *anAlertView;
    UIView *footer;
    UIImageView *arSplash;
    //DTIActivityIndicatorView *activityIndicator;
    DotActivityIndicatorView* indicatorView;
    UIVisualEffectView *blurEffectView;
    BOOL isPaused;
    BOOL toastVisible;
    BOOL playActive;
    BOOL alertShown;
    MODELKeys *modelKeys;

    
}

@property (weak, nonatomic) IBOutlet UIImageView *ARViewPlaceholder;
@property (weak, nonatomic) IBOutlet UIImageView *info;
@property (weak, nonatomic) IBOutlet UIImageView *flashOn;
@property (weak, nonatomic) IBOutlet UIImageView *feed;
@property (weak, nonatomic) IBOutlet UIView *buttonsOverlay;

@property (retain, nonatomic) MODELAugment *loadingAugment;
@property (retain, nonatomic) NSMutableDictionary *augmentsByTrackableID;
@property (retain, nonatomic) NSString *kAccessKey;
@property (retain, nonatomic) NSString *kSecretKey;
@property (retain, nonatomic) NSString *kLicenseKey;
@property (retain, nonatomic) NSString *agencyLicenseKey;
@property (retain, nonatomic) VideoHelper *loadVideoHelper;

@end

@implementation VCMotionPrint

NSInteger count =0;

@synthesize tapGestureRecognizer, vapp, viewRender,closeButton;
@synthesize  lastTargetIDScanned,trackableID,loadVideoHelper,currentTimeMillis,loadingVideoDuration,initTimer,isAugmentDataDownloaded;

//-------------------------------VIEWCONTROLLER ROTATION MGMT METHODS-----------'

#pragma mark - Autorotation
- (NSUInteger)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)navigationControllerPreferredInterfaceOrientationForPresentation:(UINavigationController *)navigationController{
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}


- (BOOL) isVisualSearchOn {
    return isVisualSearchOn;
}

- (CGRect)getCurrentARViewFrame{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGRect viewFrame = screenBounds;
    
    // If this device has a retina display, scale the view bounds
    // for the AR (OpenGL) view
    if (YES == vapp.isRetinaDisplay) {
        viewFrame.size.width *= [UIScreen mainScreen].nativeScale;
        viewFrame.size.height *= [UIScreen mainScreen].nativeScale;
    }
    return viewFrame;
}


- (void) setVisualSearchOn:(BOOL) isOn {
    isVisualSearchOn = isOn;
    if (isOn) {
        [self scanlineStart];
    } else {
        [self scanlineStop];
    }
}

-(void)showUIAlertFromErrorCode:(int)code{
    if (lastErrorCode == code)
    {
        // we don't want to show twice the same error
        return;
    }
    lastErrorCode = code;
    
    NSString *title = nil;
    NSString *message = nil;
    
    if (code == Vuforia::TargetFinder::UPDATE_ERROR_NO_NETWORK_CONNECTION)
    {
        title = @"Network Unavailable";
        message = @"Please check your internet connection and try again.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_REQUEST_TIMEOUT)
    {
        title = @"Request Timeout";
        message = @"The network request has timed out, please check your internet connection and try again.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_SERVICE_NOT_AVAILABLE)
    {
        title = @"Service Unavailable";
        message = @"The cloud recognition service is unavailable, please try again later.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_UPDATE_SDK)
    {
        title = @"Unsupported Version";
        message = @"The application is using an unsupported version of Vuforia.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_TIMESTAMP_OUT_OF_RANGE)
    {
        title = @"Clock Sync Error";
        message = @"Please update the date and time and try again.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_AUTHORIZATION_FAILED)
    {
        title = @"Authorization Error";
        message = @"The cloud recognition service access keys are incorrect or have expired.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_PROJECT_SUSPENDED)
    {
        title = @"Authorization Error";
        message = @"The cloud recognition service has been suspended.";
    }
    else if (code == Vuforia::TargetFinder::UPDATE_ERROR_BAD_FRAME_QUALITY)
    {
        title = @"Poor Camera Image";
        message = @"The camera does not have enough detail, please try again later";
    }
    else
    {
        title = @"Unknown error";
        message = [NSString stringWithFormat:@"An unknown error has occurred (Code %d)", code];
    }
    
    //  Call the UIAlert on the main thread to avoid undesired behaviors
    dispatch_async( dispatch_get_main_queue(), ^{
        if (title && message)
        {
            UIAlertView *anAlertView = [[UIAlertView alloc] initWithTitle:title
                                                                  message:message
                                                                 delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
            [anAlertView show];
        }
    });
}

- (void) hideLoadingAnimation {
    UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *)[viewRender viewWithTag:1];
}



//-------------------------------VIEWCONTROLLER LIFECYCLE METHODS-----------'
- (id)initWithAgencyId:(NSString*)agencyId{
    self = [[VCMotionPrint alloc] initWithNibName:@"VCMotionPrint" bundle:nil];
    self.agencyLicenseKey = agencyId;
    return self;
}
//--------------------------------------

- (void)loadView{
    // Custom initialization
    self.title = @"WOWSOME";
    
    if (self.ARViewPlaceholder != nil) {
        [self.ARViewPlaceholder removeFromSuperview];
        self.ARViewPlaceholder = nil;
    }
    NSLog(@"Load view called");
    scanningMode = YES;
    isVisualSearchOn = NO;
    lastTargetIDScanned = nil;
    
    extendedTrackingEnabled = NO;
    continuousAutofocusEnabled = YES;
    flashEnabled = NO;
    playFullscreenEnabled = NO;
    frontCameraEnabled = NO;
    vapp = [[SampleApplicationSession alloc] initWithDelegate:self];
    
    CGRect viewFrame = [self getCurrentARViewFrame];
    
    [_buttonsOverlay removeFromSuperview];
    
    viewRender = [[ViewRender alloc] initWithFrame:viewFrame rootViewController:self appSession:vapp];
    
    [self setView:viewRender];
    
    
    
    //    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //    appDelegate.glResourceHandler = viewRender;
    
    
    // [_info setHidden:NO];
    
    
    // double tap used to also trigger the menu
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doubleTapGestureAction:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTap];
    
    // a single tap will trigger a single autofocus operation
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    if (doubleTap != NULL) {
        [tapGestureRecognizer requireGestureRecognizerToFail:doubleTap];
    }
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureAction:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRight];
    [self scanlineCreate]; //CUSTOM SCANLINE
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissARViewController)
                                                 name:@"kDismissARViewController"
                                               object:nil];
    
    // we use the iOS notification to pause/resume the AR when the application goes (or come back from) background
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(pauseAR)
     name:UIApplicationWillResignActiveNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(resumeAR)
     name:UIApplicationDidBecomeActiveNotification
     object:nil];
    //
    //    // initialize AR
    //    if(YES){
    //        _kAccessKey     = @"6d3551613e3cd949cc0cbd1c6ef1f4cb37465b5d";
    //        _kSecretKey     = @"ed80b532f42c0548cfc2ce253d7cd15e1a098c4e";
    //        _kLicenseKey    = @"Ac+Ds6z/////AAAAGdZObqvZh0kOplritC725R+K1Z8oK3hvjq3/R9+RkWSxoOTbzcbICgZCVfc16VuKaklmCkuXwR5j+cdWc96/Ot2deBsXOh8JBeHVfDtJ/xtsCxsk/EC2Nq58EoELwhfgDV6Dqp2l4vwt0tRAmBNTNK3fSDjwfqaBU0l4BHG1YJhL9wgQC8RhHO7bRqS2hQPu+tvlVRIzh+7QnfpOtR4eoommXDnWDrcBe/dYjCUH3NslNyAMy2/vjfI++Uz7JRrBdCDs0lwgv+zSNC+z+bXnQSr+DRmkzuaSdFRnGk3X+/tSiEaPjGo7/I4umTiocGzj3K7CMPZVMy5zlkwAppuLDav0JNeNJLrZxaxMEfCnC7QG";
    //        [vapp setGivenLicenseKey:_kLicenseKey];
    //        [vapp initAR:Vuforia::GL_20 orientation:self.interfaceOrientation];
    //
    //    }
    //else{
   // _agencyLicenseKey = @"OsbanwY4qc91j1U6i29AEL3a6kMCeeTy";
   // [self getARKeys];
    [self getKeys];

    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    //}
    // show loading animation while AR is being initialized
    [self showLoadingAnimation];
    [self IsFooterVisible:NO];
}

- (void)viewDidLoad{
    NSLog(@"View created");
    [super viewDidLoad];
    //[viewRender prepare:@"https://s3-ap-southeast-1.amazonaws.com/testingcampaigns/benz/FinalMercedes800.mp4"];
    
    // we set the UINavigationControllerDelegate
    // so that we can enforce portrait only for this view controller
    self.navigationController.delegate = (id<UINavigationControllerDelegate>)self;
    
    
    
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    NSLog(@"self.navigationController.navigationBarHidden: %s", self.navigationController.navigationBarHidden ? "Yes" : "No");
    lastErrorCode = 99;
    
    _loadingAugment = [self createLoadingAugment];
    [viewRender setLoadingBarAugment:_loadingAugment];
    
    //    if([_LoadVideoHelper getStatus]==READY)
    //    {
    //        [_LoadVideoHelper pause];
    //
    //    }
    
    self.augmentsByTrackableID = [[NSMutableDictionary alloc] init];
    
    
    UIImage *closeButtonImage =  [VCMotionPrint getImage:@"close"];//[UIImage imageNamed:@"button_close_normal.png"];
    //UIImage *closeButtonTappedImage = [VCMotionPrint getImage:@"button_close_normal.png"];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
        CGRect aRect = CGRectMake(self.view.frame.size.width -(closeButtonImage.size.width + 10),
                                  10,
                                  closeButtonImage.size.width,
                                  closeButtonImage.size.height);
    
//    CGRect aRect = CGRectMake((screenWidth+self.view.frame.size.width)/2 - (closeButtonImage.size.width/2),
//                              0,
//                              closeButtonImage.size.width,
//                              closeButtonImage.size.height);
    
    
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = aRect;
    //    closeButton.center=self.view.center;
    
    [closeButton setImage:closeButtonImage forState:UIControlStateNormal];
   // [closeButton setImage:closeButtonTappedImage forState:UIControlStateHighlighted];
    
    closeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    [closeButton addTarget:self action:@selector(closeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeButton];
    [closeButton setHidden:YES];
    //[self presentViewController:[[PBJViewController alloc] init] animated:NO completion:nil];
    
}

- (void)viewDidAppear:(BOOL)animated{
    initTimer=[self setTimer];

    isViewControllerInForeground = YES;
    if(isPaused){
        [self resumeAR];
        isPaused=NO;
    }NSLog(@"Resume AR called");
}


- (void)viewDidDisappear:(BOOL)animated {
    // This is called when the full time player is being displayed
    // so we check the boolean to avoid shutting down AR
    //[viewRender pauseAugment];
    NSLog(@"View disappeared Paused");
    isViewControllerInForeground = NO;
    isPaused=YES;
    [self pauseAR];
    
}



//---------------------------------VUFORIA TRACKER MNGMT METHODS------------------------------------------

#pragma mark - SampleApplicationControl

// Initialize the application trackers
- (bool) doInitTrackers {
    // Initialize the image tracker
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::Tracker* trackerBase = trackerManager.initTracker(Vuforia::ObjectTracker::getClassType());
    if (trackerBase == NULL)
    {
        NSLog(@"Failed to initialize ObjectTracker.");
        return false;
    }
    return true;
    
    
    // Set the visual search credentials:
    Vuforia::TargetFinder* targetFinder = static_cast<Vuforia::ObjectTracker*>(trackerBase)->getTargetFinder();
    if (targetFinder == NULL)
    {
        NSLog(@"Failed to get target finder.");
        return NO;
    }
    
    NSLog(@"Successfully initialized ObjectTracker.");
    return YES;
}

- (bool) doLoadTrackersData {
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if (objectTracker == NULL)
    {
        NSLog(@">doLoadTrackersData>Failed to load tracking data set because the ImageTracker has not been initialized.");
        return NO;
        
    }
    
    // Initialize visual search:
    Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
    if (targetFinder == NULL)
    {
        NSLog(@">doLoadTrackersData>Failed to get target finder.");
        return NO;
    }
    
    NSDate *start = [NSDate date];
    
    // Start initialization:
    if (targetFinder->startInit([modelKeys.vuforiaAccesskey UTF8String], [modelKeys.vuforiaSecretkey UTF8String]))
    {
        targetFinder->waitUntilInitFinished();
        
        NSDate *methodFinish = [NSDate date];
        NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:start];
        
        NSLog(@"waitUntilInitFinished Execution Time: %lf", executionTime);
    }
    
    int resultCode = targetFinder->getInitState();
    if ( resultCode != Vuforia::TargetFinder::INIT_SUCCESS)
    {
        NSLog(@">doLoadTrackersData>Failed to initialize target finder.");
        if (resultCode == Vuforia::TargetFinder::INIT_ERROR_NO_NETWORK_CONNECTION) {
            NSLog(@"CloudReco error:Vuforia::TargetFinder::INIT_ERROR_NO_NETWORK_CONNECTION");
        } else if (resultCode == Vuforia::TargetFinder::INIT_ERROR_SERVICE_NOT_AVAILABLE) {
            NSLog(@"CloudReco error:Vuforia::TargetFinder::INIT_ERROR_SERVICE_NOT_AVAILABLE");
        } else {
            NSLog(@"CloudReco error:%d", resultCode);
        }
        
        int initErrorCode;
        if(resultCode == Vuforia::TargetFinder::INIT_ERROR_NO_NETWORK_CONNECTION)
        {
            initErrorCode = Vuforia::TargetFinder::UPDATE_ERROR_NO_NETWORK_CONNECTION;
        }
        else
        {
            initErrorCode = Vuforia::TargetFinder::UPDATE_ERROR_SERVICE_NOT_AVAILABLE;
        }
        [self showUIAlertFromErrorCode: initErrorCode];
        return NO;
    } else {
        NSLog(@">doLoadTrackersData>target finder initialized");
    }
    
    return YES;
}

- (bool) doStartTrackers {
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(
                                                                                 trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if (objectTracker == 0) {
        NSLog(@"Failed to start Object Tracker, as it is null.");
        return NO;
    }
    objectTracker->start();
    
    // Start cloud based recognition if we are in scanning mode:
    if (scanningMode)
    {
        Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
       // targetFinder->setUIPointColor(1,0,0);
        [self scanlineStart];
        if (targetFinder != 0) {
            isVisualSearchOn = targetFinder->startRecognition();
            Vuforia::setHint(Vuforia::HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 2);
        }
    }
    return YES;
    
    // Set the number of simultaneous targets to two
    
    
    //    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::Tracker* tracker = trackerManager.getTracker(Vuforia::ObjectTracker::getClassType());
    if(tracker == 0) {
        return false;
    }
    tracker->start();
    
    return true;
}


- (bool) doStopTrackers {
    // Stop the tracker
    // Stop the tracker:
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(
                                                                                 trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if(objectTracker != 0) {
        objectTracker->stop();
        
        // Stop cloud based recognition:
        Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
        if (targetFinder != 0) {
            isVisualSearchOn = !targetFinder->stop();
        }
    }
    return YES;
    
}

- (bool) doUnloadTrackersData {
    
    
    if (dataSet != NULL) {
        // Get the image tracker:
        Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
        Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
        
        if (objectTracker == NULL)
        {
            NSLog(@"Failed to unload tracking data set because the ImageTracker has not been initialized.");
            return false;
        }
        // Activate the data set:
        if (!objectTracker->deactivateDataSet(dataSet))
        {
            NSLog(@"Failed to deactivate data set.");
            return false;
        }
        // Activate the data set:
        if (!objectTracker->destroyDataSet(dataSet))
        {
            NSLog(@"Failed to destroy data set.");
            return false;
        }
        dataSet = NULL;
    }
    return true;
    // Get the image tracker:
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    if (objectTracker == NULL)
    {
        NSLog(@"Failed to unload tracking data set because the ObjectTracker has not been initialized.");
        return NO;
    }
    
    // Deinitialize visual search:
    Vuforia::TargetFinder* finder = objectTracker->getTargetFinder();
    finder->deinit();
    return YES;
}

//---------------------------------VUFORIA TRACKER MNGMT METHODS------------------------------------------





//##################################   FRAMEWORK     ####################################################################
//-------------------------------VUFORIA CALLBACK METHODS-----------'

- (void) pauseAR {
    [self showBlurEffect];
    [viewRender pauseAugment];
    NSLog(@"AR paused");
    [viewRender dismissPlayers];
    NSError * error = nil;
    if (![vapp pauseAR:&error]) {
        NSLog(@"Error pausing AR:%@", [error description]);
    }
}

- (void) resumeAR {
    
    NSLog(@"AR resumed");
    [viewRender preparePlayers];
    
    [self removeBlurEffectAfterDelay];
    NSError * error = nil;
    if(! [vapp resumeAR:&error]) {
        NSLog(@"Error resuming AR:%@", [error description]);
    }
    // on resume, we reset the flash
    Vuforia::CameraDevice::getInstance().setFlashTorchMode(false);
    flashEnabled = NO;
    if(viewRender!=nil){
        [viewRender setPlayOnFocus:YES];
    }
    if(!isWifiEnabled){
        //[self getARKeys];
        
    }
}



// update from the Vuforia loop
- (void) onVuforiaUpdate: (Vuforia::State *) state {
    //NSLog(@"onVuforiaUpdate called");
    // Get the tracker manager:
    
    // [viewRender inNetworkConnection];
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    
    // Get the image tracker:
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    // Get the target finder:
    Vuforia::TargetFinder* finder = objectTracker->getTargetFinder();
    
    // Check if there are new results available:
    const int statusCode = finder->updateSearchResults();
    //NSLog(@"StatusCode:%d", statusCode);
    [self isWifiEnabled:YES];
    if (statusCode < 0)
    {
        // Show a message if we encountered an error:
        //        NSLog(@"update search result failed:%d", statusCode);
        if (statusCode == Vuforia::TargetFinder::UPDATE_ERROR_NO_NETWORK_CONNECTION || (statusCode == Vuforia::TargetFinder::UPDATE_ERROR_SERVICE_NOT_AVAILABLE) ) {
            
            NSLog(@"No network connection error");
            [self notifyNetworkStatus:1000];
            [viewRender stopPlayback];
            
            return;
        }
    }else if( (statusCode == Vuforia::TargetFinder::UPDATE_NO_MATCH))
    {
        
        //[self showInvalidTargetMessage];
        NSLog(@"Play Active set in status code ::: NO");
        
        
        
    }else if (statusCode == Vuforia::TargetFinder::UPDATE_RESULTS_AVAILABLE){
        
        [self isWifiEnabled:YES];
        int resultCount = finder->getResultCount();
        NSLog(@"before for loop %d",resultCount);
        
        if(resultCount==0){
            
            // [viewRender setModelAugment:nil];
            //[viewRender enterScanningMode];
            
            return;
        }
        
        
        NSString *currentTrackableID =nil;
        // Iterate through the new results:
        for (int i = 0; i < resultCount; ++i){
            playActive=YES;
            
            
            //NSLog(@"Play Active:::YES");
            
            if(toastVisible  && ((viewRender != nil) &&  [viewRender isCurrentlyRendering])){
                [self.view hideToastActivity];
                
                //NSLog(@"Toast Hidden");
            }
            
            const Vuforia::TargetSearchResult*  result          = finder->getResult(i);
            int                                 trackableRating = result->getTrackingRating();
            NSString*                           trackableID     = [NSString stringWithFormat:@"%s",result->getUniqueTargetId()];
            
            
            
            
            MODELAugment*                       modelAugment    = [self.augmentsByTrackableID objectForKey:trackableID];
            NSLog(@"just identified a new trackable® %@", [NSThread currentThread]);
            if (trackableRating > 0){
                
                [viewRender resetLoadingAugment];
                loadingVideoDuration = [self setTimer];
                long idleTimer = loadingVideoDuration - currentTimeMillis;
                idleTimer/=1000;
                // NSLog(@"Idle duration:::%ld",idleTimer);
                if(idleTimer<100){
                    NSNumber *loadingBarDuration=[NSNumber numberWithLong:idleTimer];
                    [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onNewCampaignScanned:trackableID idleTimer:loadingBarDuration];
                }
                //                NSNumber *idleDuration=[NSNumber numberWithLong:idleTimer];
                //                [[UTILAnalyticsManager sharedInstance] onNewCampaignScanned:trackableID idleTimer:idleDuration];
                if (modelAugment==nil || modelAugment.canBeRendered){
                    
                    
                    Vuforia::Trackable*                 newTrackable    = finder->enableTracking(*result);
                    Vuforia::ImageTarget*               imageTarget     = (Vuforia::ImageTarget*)newTrackable;
                    Vuforia::Vec3F size = imageTarget->getSize();
                    int trackableWidth = size.data[0];
                    int trackableHeight = size.data[1];
                    NSLog(@"New trackable >> '%s' << identified with rating '%d'.", newTrackable->getName(), trackableRating);
                    
                    __block MODELAugment *updateAugment=nil;
                    DFFeeds *dfFeeds = [[DFFeeds alloc] init];
                    
                    if (modelAugment==nil) {
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            NSLog(@"getMetadata %@", [NSThread currentThread]);
                            isAugmentDataDownloaded = NO;
                            NSLog(@"augment not yet downloaded");
                            [dfFeeds getMetadata:trackableID licenseKey:_agencyLicenseKey responseHandler:^(MODELResultSet *resultSet) {
                                
                                NSLog(@"After response from Datafetch %@", [NSThread currentThread]);
                                if([resultSet hasError]){
                                    if([resultSet.error isEqualToString:@"100"] || [resultSet.error isEqualToString:@"200"]) {
                                        MODELAugment *augment = [[MODELAugment alloc] init];
                                        augment.trackableId = trackableID;
                                        augment.canBeRendered = NO;
                                        [self addAugmentToCache:augment];
                                        [self showInvalidTargetMessage];
                                        playActive=NO;
                                        
                                        finder->clearTrackables();
                                    }
                                    
                                    NSLog(@"Error fetching ModelAugment from Datafetch %@", [resultSet getSingleLineUIMessage]);
                                    
                                }else{
                                    MODELAugment *augment = (MODELAugment*)resultSet.data;
                                    augment.canBeRendered = YES;
                                    augment.isMediaDownloaded = NO;
                                    
                                    augment.trackableId = trackableID;
                                    [self addAugmentToCache:augment];
                                    
                                    
                                    NSLog(@"Trackable found with %@ and id %@",augment,trackableID);
                                    MODELMedia *mainContentMedia = augment.mainContent;
                                    if (mainContentMedia.mediaType == VIDEO && mainContentMedia.cloudURL != nil && mainContentMedia.cloudURL.length>0) {
                                        
                                        [AsyncImageDownloader updateURLForAWSAccess:mainContentMedia.cloudURL modelKeys:modelKeys awsTask: ^id(AWSTask *task)
                                         {
                                             if (task.error) {
                                                 NSLog(@"AWS Error: %@",task.error);
                                             } else {
                                                 NSURL *finalURL = task.result;
                                                 mainContentMedia.cloudURL = finalURL.absoluteString;
                                             }
                                             return nil;
                                         }];
                                        
                                    }
                                    
                                    NSMutableArray *imagesMedia = [augment getImagesList];
                                    if(imagesMedia!=nil && imagesMedia.count>0){
                                        //  const Vuforia::TrackableResult  *trackableResult = state->getTrackableResult(0);
                                        //  Vuforia::Matrix44F  modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(trackableResult->getPose());
                                        [viewRender setLoadingBarAugment:_loadingAugment];
                                        [[[HLPRAugmentDownloader alloc]initWithAugment:augment modelKeys:modelKeys successBlock: ^(MODELAugment *augment){
                                            //[[[HLPRAugmentDownloader alloc]initWithAugment:augment successBlock:^(MODELAugment *augment) {
                                            NSLog(@"Success DOWNLOADING elements:%@ %@",augment,self);
                                            augment.isMediaDownloaded = YES;
                                            NSLog(@"Success DOWNLOADING elements11:%@ %@",augment,self);
                                            NSLog(@"Success DOWNLOADING elements22:%@ %@",augment,self);
                                            
                                            [viewRender setModelAugment:augment];
                                            NSLog(@"THread ::::VController2::::%@", [NSThread currentThread]);
                                            
                                            NSLog(@"Success DOWNLOADING elements33:%@ %@",augment,self);
                                            
                                            
                                        } failBlock:^(MODELAugment *augment) {
                                            augment.isMediaDownloaded = NO;
                                            NSLog(@"FAILED DOWNLOADING:%@",augment);
                                            
                                        }] startDownload];
                                        
                                    }else{
                                        augment.isMediaDownloaded = YES;
                                        
                                        [viewRender setModelAugment:augment];
                                        NSLog(@"THread ::::VController3::::%@", [NSThread currentThread]);
                                        
                                    }
                                    
                                }
                                
                                
                            }];
                            
                            //                            if(trackableHeight>trackableWidth){
                            //                                self.loadingAugment.mainContent.width=1;
                            //                                self.loadingAugment.mainContent.height=0.5;
                            //                            }else{
                            //                                self.loadingAugment.mainContent.width=1;
                            //                                self.loadingAugment.mainContent.height=1;
                            //
                            //                            }
                            //
                            //                            [viewRender setLoadingBarAugment:self.loadingAugment];
                        });
                        //
                    }
                    else{
                        
                        NSLog(@"augment already downloaded");
                        NSLog(@"%d",modelAugment.isMediaDownloaded);
                        if(modelAugment.isMediaDownloaded){
                            NSLog(@"augment media also downloaded");
                            //  [viewRender resetLoadingAugment];
                            [dfFeeds getMetadata:trackableID licenseKey:_agencyLicenseKey responseHandler:^(MODELResultSet *resultSet) {}];
                            [viewRender setModelAugment:modelAugment];
                            isAugmentDataDownloaded = YES ;
                            NSLog(@"THread ::::VController1::::%@", [NSThread currentThread]);
                            
                        }else{
                            //                            modelAugment=nil;
                            NSLog(@"augment media not yet downloaded");
                            
                            
                            NSMutableArray *imagesMedia = [modelAugment getImagesList];
                            if(imagesMedia!=nil && imagesMedia.count>0){
                                //  const Vuforia::TrackableResult  *trackableResult = state->getTrackableResult(0);
                                //  Vuforia::Matrix44F  modelViewMatrix = Vuforia::Tool::convertPose2GLMatrix(trackableResult->getPose());
                                [viewRender setLoadingBarAugment:_loadingAugment];
                                
                                //[[[HLPRAugmentDownloader alloc]initWithAugment:modelAugment successBlock:^(MODELAugment *modelAugment) {
                                [[[HLPRAugmentDownloader alloc]initWithAugment:modelAugment modelKeys:modelKeys successBlock:^(MODELAugment *modelAugment) {
                                    
                                    NSLog(@"Success DOWNLOADING:%@ self::%@",modelAugment,self);
                                    modelAugment.isMediaDownloaded = YES;
                                    [viewRender setModelAugment:modelAugment];
                                    NSLog(@"THread ::::VController:4:::%@", [NSThread currentThread]);
                                    
                                    
                                } failBlock:^(MODELAugment *modelAugment) {
                                    modelAugment.isMediaDownloaded = NO;
                                    NSLog(@"FAILED DOWNLOADING:%@",modelAugment);
                                    
                                }] startDownload];
                            }
                            
                        }
                        
                    }
                    
                }else{
                    NSLog(@"augment not to be rendered");
                    
                }
            }else{
                
                
            }
            
        }
    }else{
        
        playActive=NO;
        [self showInvalidTargetMessage];
        
        if(([viewRender isCurrentlyRendering]==YES) && toastVisible && viewRender!=nil){
            [self.view hideToastActivity];
            // NSLog(@"Toast hidden >>>>>>");
        }
        
    }
    
}

//---------------------------------VUFORIA LOAD ANIMATION METHODS---------------------------------------------


#pragma mark - loading animation

- (void) onInitARDone:(NSError *)initError {
    
    [self stopAnimation];
    
    UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *)[viewRender viewWithTag:1];
    [UIView transitionWithView:self.view
                      duration:1.00f
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^(void){
                        arSplash.alpha = 0.0f;
                    }
                    completion:^(BOOL finished){
                        [arSplash removeFromSuperview];
                        [[self.view viewWithTag:3000] setHidden:NO];
                        [[self.view viewWithTag:3001] setHidden:NO];
                        [[self.view viewWithTag:3002] setHidden:NO];
                    }];
    
    [indicatorView removeFromSuperview];
    
    
    if (initError == nil) {
        currentTimeMillis=[self setTimer];
        initTimer = currentTimeMillis - initTimer;
        initTimer/=1000;
        // NSLog(@"initDuration::::%ld",initTimer);
        NSNumber *duration=[NSNumber numberWithLong:initTimer];
        
        [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] onARLaunched:duration];
        NSError * error = nil;
        [vapp startAR:Vuforia::CameraDevice::CAMERA_DIRECTION_BACK error:&error];
        
        // by default, we try to set the continuous auto focus mode
        continuousAutofocusEnabled = Vuforia::CameraDevice::getInstance().setFocusMode(Vuforia::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO);
        
        
        //        dispatch_async( dispatch_get_main_queue(), ^{
        //            UIView *playerView = [[PBJViewController alloc] init].view;
        //            [playerView setTag:2000];
        //            [viewRender addSubview:playerView];
        //        });
        
    } else {
        NSLog(@"Error initializing AR:%@", [initError description]);
        //[self notifyNetworkStatus:2000];
    }
    //        dispatch_async( dispatch_get_main_queue(), ^{
    //
    //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
    //                                                            message:[initError localizedDescription]
    //                                                           delegate:self
    //                                                  cancelButtonTitle:@"Retry"
    //                                                  otherButtonTitles:nil];
    //            [alert show];
    //        });
    //    }
    //[self IsFooterVisible:YES];
    
}

- (void) showLoadingAnimation {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    UIImage *arsplashimage = [VCMotionPrint getImage:@"arsplash"];//[UIImage imageNamed:@"arSplash"];
    NSLog(@"AR splash");
    arSplash = [[UIImageView alloc] initWithImage:arsplashimage];
    [arSplash setBackgroundColor:[UIColor whiteColor]];
    [arSplash setContentMode:UIViewContentModeScaleAspectFit];
    [arSplash setClipsToBounds:YES];
    [arSplash sizeToFit];
    [arSplash setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [self.view addSubview:arSplash];
    [self.view bringSubviewToFront:arSplash];
    
    //LoadingBar
    float frameWidth=80;
    float frameHeight=30;
    float deviceWidth=((screenWidth/2)-(frameWidth/2));
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        //activityIndicator = [[DTIActivityIndicatorView alloc]initWithFrame:CGRectMake(deviceWidth, screenHeight*0.7, 80, 30)];
        indicatorView = [[DotActivityIndicatorView alloc] initWithFrame:CGRectMake(deviceWidth, screenHeight*0.7, 80, 30)];
        
    }else{
        //activityIndicator = [[DTIActivityIndicatorView alloc]initWithFrame:CGRectMake(deviceWidth, screenHeight*0.65, 80, 30)];
        indicatorView = [[DotActivityIndicatorView alloc] initWithFrame:CGRectMake(deviceWidth, screenHeight*0.65, 80, 30)];
    }
    
    [self.view addSubview:indicatorView];
    indicatorView.dotParms = [self loadDotActivityIndicatorParms];
    [self startAnimation];
    
    
    
    //    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //    CGRect frame = activityIndicator.frame;
    //    frame.origin.x = viewRender.frame.size.width/2- (frame.size.width/2);
    //    frame.origin.y = viewRender.frame.size.height- (0.25*viewRender.frame.size.height);
    //    [activityIndicator setFrame:frame];
    //    [viewRender addSubview:activityIndicator];
    //    [activityIndicator startAnimating];
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1000){
        //do nothing
        alertShown=NO;
        
    }else if(alertView.tag==2000){
        alertShown=NO;
        [self startAnimation];
        //[self getARKeys];
        [self getKeys];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kDismissARViewController" object:nil];
    }
}


//##################################   FRAMEWORK  NO edits   ##################################################


- (void)dismissARViewController{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void) toggleVisualSearch {    [self toggleVisualSearch:isVisualSearchOn];
}

- (void) toggleVisualSearch:(BOOL)visualSearchOn
{
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    if (objectTracker == 0) {
        NSLog(@"Failed to toggle Visual Search, as Object Tracker is null.");
        return;
    }
    
    Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
    if (visualSearchOn == NO)
    {
        NSLog(@"Starting target finder");
        targetFinder->startRecognition();
        isVisualSearchOn = YES;
    }
    else
    {
        NSLog(@"Stopping target finder");
        targetFinder->stop();
        isVisualSearchOn = NO;
    }
}


//----------------------------------------------




// deinitialize your trackers
- (bool) doDeinitTrackers {
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    trackerManager.deinitTracker(Vuforia::ObjectTracker::getClassType());
    return true;
}


- (void)autofocus:(UITapGestureRecognizer *)sender{
    [self performSelector:@selector(cameraPerformAutoFocus) withObject:nil afterDelay:.4];
}

- (void)cameraPerformAutoFocus{
    Vuforia::CameraDevice::getInstance().setFocusMode(Vuforia::CameraDevice::FOCUS_MODE_TRIGGERAUTO);
}




// Load the image tracker data set
- (BOOL)loadAndActivateImageTrackerDataSet:(NSString*)dataFile{
    NSLog(@"loadAndActivateImageTrackerDataSet (%@)", dataFile);
    BOOL ret = YES;
    dataSet = NULL;
    
    // Get the Vuforia tracker manager image tracker
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    if (NULL == objectTracker) {
        NSLog(@"ERROR: failed to get the ImageTracker from the tracker manager");
        ret = NO;
    } else {
        dataSet = objectTracker->createDataSet();
        
        if (NULL != dataSet) {
            // Load the data set from the app's resources location
            if (!dataSet->load([dataFile cStringUsingEncoding:NSASCIIStringEncoding], Vuforia::STORAGE_APPRESOURCE)) {
                NSLog(@"ERROR: failed to load data set");
                objectTracker->destroyDataSet(dataSet);
                dataSet = NULL;
                ret = NO;
            } else {
                // Activate the data set
                if (objectTracker->activateDataSet(dataSet)) {
                    NSLog(@"INFO: successfully activated data set");
                }
                else {
                    NSLog(@"ERROR: failed to activate data set");
                    ret = NO;
                }
            }
        }
        else {
            NSLog(@"ERROR: failed to create data set");
            ret = NO;
        }
        
    }
    
    return ret;
}

- (BOOL) setExtendedTrackingForDataSet:(Vuforia::DataSet *)theDataSet start:(BOOL) start {
    BOOL result = YES;
    for (int tIdx = 0; tIdx < theDataSet->getNumTrackables(); tIdx++) {
        Vuforia::Trackable* trackable = theDataSet->getTrackable(tIdx);
        if (start) {
            if (!trackable->startExtendedTracking())
            {
                NSLog(@"Failed to start extended tracking on: %s", trackable->getName());
                result = false;
            }
        } else {
            if (!trackable->stopExtendedTracking())
            {
                NSLog(@"Failed to stop extended tracking on: %s", trackable->getName());
                result = false;
            }
        }
    }
    return result;
    /*  Vuforia::TargetFinder* targetFinder = objectTracker->getTargetFinder();
     int nbTargets = targetFinder->getNumImageTargets();
     for(int idx = 0; idx < nbTargets ; idx++) {
     Vuforia::ImageTarget * it = targetFinder->getImageTarget(idx);
     if (it != NULL) {
     if (isActive) {
     it->startExtendedTracking();
     } else {
     it->stopExtendedTracking();
     }
     }
     }*/
}

- (void) setOffTargetTracking:(BOOL) isActive {
    Vuforia::TrackerManager& trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker* objectTracker = static_cast<Vuforia::ObjectTracker*>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    
    if (objectTracker == 0) {
        NSLog(@"Failed to enable Extended Tracking, as the Object Tracker is null.");
        return;
    }
    
    
}



//-------------------------------VIEWCONTROLLER HELPER METHODS-----------'

- (MODELAugment*) createLoadingAugment{
    
    
    MODELMedia *mainContentMedia = [[MODELMedia alloc] init];
    mainContentMedia.mediaType = VIDEO;
    NSString *fullPath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"loadingAnimation.mp4"];
    NSURL* mediaURL = [[NSURL alloc] initFileURLWithPath:fullPath];
    NSLog(@">>>>LoadingBar::::%@",mediaURL);
    mainContentMedia.cloudURL=mediaURL.absoluteString;
    mainContentMedia.x =0.0f;
    mainContentMedia.y =0.0f;
    mainContentMedia.z = 1.0f;
    mainContentMedia.width = 1.0f;
    mainContentMedia.height = 1.0f;
    
    
    
    MODELAugment *modelAugment = [[MODELAugment alloc] init];
    modelAugment.mainContent= mainContentMedia;
    modelAugment.augmentType = AUGMENT_VIDEO;
    
    
    
    
    
    loadVideoHelper=[[VideoHelper alloc]initWithRootViewController:self withCallBack:nil];
    NSLog(@"VideoHelper Created in createLoading augemnt");
    
    modelAugment.mainContent.videoHelper= loadVideoHelper;
    
    // [loadVideoHelper setTextureId:[viewRender createOpenGLTexture]];
    [loadVideoHelper resetData];
    [loadVideoHelper load:modelAugment.mainContent.cloudURL playImmediately:NO fromPosition:0.0f];
    //delegate for Mediastatus
    loadVideoHelper.delegate=self;
    [loadVideoHelper startSampleProcess];
    
    
    
    return modelAugment;
    
}

- (void)swipeGestureAction:(UISwipeGestureRecognizer*)gesture{}

- (void)doubleTapGestureAction:(UISwipeGestureRecognizer*)gesture{}

// tap handler
- (void)handleTap:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        [viewRender addSubview:_buttonsOverlay];
        [viewRender bringSubviewToFront:_buttonsOverlay];
        // handling code
        CGPoint touchPoint = [sender locationInView:viewRender];
        [viewRender handleTouchPoint:touchPoint];
    }
    
    [self autofocus:sender];
}


- (NSString *) lastTargetIDScanned {
    return lastTargetIDScanned;
}
- (void) setLastTargetIDScanned:(NSString *) targetId {
    if (lastTargetIDScanned != nil) {
        lastTargetIDScanned = nil;
    }
    if (targetId != nil) {
        lastTargetIDScanned = [NSString stringWithString:targetId];
    }
}

-(void)UpdateMediaStatus:(MEDIA_STATE)mediaStatus{
}

-(void)mediaState{
    if([loadVideoHelper getStatus]==READY)
    {
        [self IsFooterVisible:NO];
        NSLog(@"PrintME");
        [loadVideoHelper pause];
        NSLog(@"Paused1") ;
    }
    
}
- (VideoHelper*)videoHelper{
    return loadVideoHelper;
}

-(void)showInvalidTargetMessage {
    lastInvalidToastShownAt = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);
    
    // NSLog(@"TOAST::::%ld-%ld=%ld",lastInvalidToastShownAt,currentTimeMillis,(lastInvalidToastShownAt-currentTimeMillis));
    
    if (viewRender != nil && ([viewRender isCurrentlyRendering]) && toastVisible &&  (CSToastManager.isToastBeingShown)==YES){
        [self.view hideToastActivity];
        //  NSLog(@"first Toast is hidden");
    }
    
    
    if(lastInvalidToastShownAt-currentTimeMillis>8000 &&(!playActive==YES) && ![viewRender isCurrentlyRendering]){
        dispatch_async( dispatch_get_main_queue(), ^{
            if (![self.view isToastBeingShown] && [self isViewControllerInForeground]) {
                [self.view makeToast:@"Find Interactive Print to UNLOCK media"
                            duration:2.0
                            position:CSToastPositionBottom];
                
            }
            
            
            toastVisible=YES;
            
            
            [NSTimer scheduledTimerWithTimeInterval:2.0f
                                             target:self
                                           selector:@selector(secondToast:)
                                           userInfo:nil
                                            repeats:YES];
            currentTimeMillis=[self setTimer];
            
            
            
        });
        
        
        
        
    }
    
    
    
    
    
    
    
}

- (BOOL)isViewControllerInForeground{
    return isViewControllerInForeground;
}


-(void)notifyNetworkStatus:(int)tag{
    [self isWifiEnabled:NO];
    // [activityIndicator stopActivity:YES];
    
    dispatch_async( dispatch_get_main_queue(), ^{
        
        
        //        if(anAlertView==nil){
        anAlertView = [[UIAlertView alloc] initWithTitle:@"Server not Reachable"
                                                 message:@"Please check your Internet Connection"
                                                delegate:self
                                       cancelButtonTitle:@"Retry"
                                       otherButtonTitles:nil];
        //  }
        [anAlertView setTag:tag];
        if(!alertShown){
            [anAlertView show];
            alertShown =YES;
            [self stopAnimation];
            if(toastVisible){
                [self.view hideToastActivity];
            }
        }
        
    });
    
}

-(void)IsFooterVisible:(BOOL)IsFooterVisible{
    if(IsFooterVisible){
        dispatch_async( dispatch_get_main_queue(), ^{
            
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            CGFloat screenHeight = screenRect.size.height;
            footer = [[UIView alloc]  initWithFrame:CGRectMake(screenWidth*0.3,0,screenWidth,screenHeight*0.1)];
            UIImage *image =  [VCMotionPrint getImage:@"footer"];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.backgroundColor=[UIColor blackColor];
            imageView.center=self.view.center;
            
            [imageView setContentMode:UIViewContentModeCenter];
            
            float y = [UIScreen mainScreen].bounds.size.height - footer.frame.size.height;
            [imageView setFrame:CGRectMake(0, y, footer.frame.size.width, footer.frame.size.height)];
            [self.view addSubview:imageView];
        });
    }
}

-(void)isWifiEnabled:(BOOL)WifiEnabled{
    
    isWifiEnabled=WifiEnabled;
}


- (void)showBlurEffect{
    //    if(blurEffectView==nil){
    //        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    //        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    //        blurEffectView.frame = self.view.bounds;
    //        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //    }
    //
    //    if(![blurEffectView isDescendantOfView:self.view])[self.view addSubview:blurEffectView];
}

- (void)removeBlurEffectAfterDelay{
    // Delay execution of my block for 1 second.
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    //        [blurEffectView removeFromSuperview];
    //    });
}
+(UIImage*)getImage:(NSString*)name{
    UIImage *image;
    if(isFrameworkMode==1){
        NSLog(@"Frameworks Splash");
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        image = [UIImage imageNamed:name inBundle:bundle compatibleWithTraitCollection:nil];
    }else{
        
        image =  [UIImage imageNamed:name];
    }
    return image;
}


-(long)setTimer{
    return ((long long)([[NSDate date] timeIntervalSince1970] * 1000.0));
    
}
-(void)secondToast:(NSTimer *)timer{
    if (viewRender != nil && ([viewRender isCurrentlyRendering]) && toastVisible) {
        [self.view hideToastActivity];
        NSLog(@"Second Toast is hidden");
        return;
    }
    
    dispatch_async( dispatch_get_main_queue(), ^{
        if (![self.view isToastBeingShown] && [self isViewControllerInForeground]) {
            toastVisible=YES;
            [self.view makeToast:@"Hold your device camera steady for best performance"
                        duration:2.0
                        position:CSToastPositionBottom];
            if(timer){
                [timer invalidate];
                
            }
        }
        
    });
    currentTimeMillis=[self setTimer];
    
    
}



- (DotActivityIndicatorParms *)loadDotActivityIndicatorParms
{
    DotActivityIndicatorParms *dotParms = [DotActivityIndicatorParms new];
    dotParms.activityViewWidth = indicatorView.frame.size.width;
    dotParms.activityViewHeight = indicatorView.frame.size.height;
    dotParms.numberOfCircles = 4;
    dotParms.internalSpacing = 3;
    dotParms.animationDelay = 0.2;
    dotParms.animationDuration = 0.5;
    dotParms.animationFromValue = 0.3;
    dotParms.defaultColor = [UIColor redColor];
    dotParms.isDataValidationEnabled = YES;
    return dotParms;
}

-(void)startAnimation{

    [indicatorView startAnimating];
}

-(void)stopAnimation{

    [indicatorView stopAnimating];
}

#pragma mark - scan line
const int VIEW_SCAN_LINE_TAG = 1111;

- (void) scanlineCreate {
    CGRect frame = [[UIScreen mainScreen] bounds];
    
    UIImageView *scanLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 50)];
    scanLineView.tag = VIEW_SCAN_LINE_TAG;
    scanLineView.contentMode = UIViewContentModeScaleToFill;
    [scanLineView setImage:[VCMotionPrint getImage:@"scanline.png"]];//[UIImage imageNamed:@"scanline.png"]];
    [scanLineView setHidden:YES];
    [self.view addSubview:scanLineView];
}

- (void) scanlineStart {
    UIView * scanLineView = [self.view viewWithTag:VIEW_SCAN_LINE_TAG];
    if (scanLineView) {
        [scanLineView setHidden:NO];
        CGRect frame = [[UIScreen mainScreen] bounds];
        scanLineView.frame = CGRectMake(0, 0, frame.size.width, 50);
        
        NSLog(@"frame: %@", NSStringFromCGRect(frame));
        CABasicAnimation *animation = [CABasicAnimation
                                       animationWithKeyPath:@"position"];
        
        animation.toValue = [NSValue valueWithCGPoint:CGPointMake(scanLineView.center.x, frame.size.height)];
        animation.autoreverses = YES;
        // we make the animation faster in landcsape mode
        animation.duration = frame.size.height > frame.size.width ? 4.0 : 2.0;
        animation.repeatCount = HUGE_VAL;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [scanLineView.layer addAnimation:animation forKey:@"position"];
    }
}

- (void) scanlineStop {
    UIView * scanLineView = [self.view viewWithTag:VIEW_SCAN_LINE_TAG];
    if (scanLineView) {
        [scanLineView setHidden:YES];
        //[scanLineView removeFromSuperview];
        [scanLineView.layer removeAllAnimations];
    }
}

- (void) scanlineUpdateRotation {
    [self scanlineStop];
    [self scanlineStart];
}

- (void)addAugmentToCache:(MODELAugment*)augment{
    
    NSInteger counter = [_augmentsByTrackableID count];
    augment.priority = ++count;
    [self.augmentsByTrackableID setObject:augment forKey:augment.trackableId];
    NSLog(@"New augment added with trackableid %@", augment.trackableId);
    NSLog(@"counter value:::%zd",counter);
    if(counter==3){
        
        MODELAugment *leastPriorityAugment = nil;
        for(id trackableId in self.augmentsByTrackableID) {
            MODELAugment *augment = [self.augmentsByTrackableID objectForKey:trackableId];
            if(leastPriorityAugment == nil){
                leastPriorityAugment = augment;
            }else if(augment.priority < leastPriorityAugment.priority){
                leastPriorityAugment = augment;
            }
        }
        
        if(leastPriorityAugment!=nil){
            NSLog(@"Trackable removed %@",leastPriorityAugment.trackableId);
            
            [self.augmentsByTrackableID removeObjectForKey:leastPriorityAugment.trackableId];
            [leastPriorityAugment releaseData];
            leastPriorityAugment=nil;
            //NSLog(@"Trackable removed %@",leastPriorityAugment.trackableId);
            return ;
            
        }
        
    }
}
- (void)addButtons{
    //  Adds close button on the upper right corner
    [closeButton setHidden:NO];
   // [addButton setHidden:NO];
    
    
}
-(void)removeButtons{
    [closeButton setHidden:YES];
    //[addButton setHidden:YES];
    
}

- (void)closeButtonTapped:(id)sender{
    NSLog(@"close button called");
    dispatch_async( dispatch_get_main_queue(), ^{
        [self scanlineStart];
    });
    [viewRender sendAnalytics];
    [viewRender publishStateChange:STATE_3D];
    [viewRender setTransitionToNormal];
    [self removeButtons];
    
}



-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender{
    [[UTILAnalyticsManager sharedInstancewithKey:modelKeys.flurryAppId] updateUserEmailId:emaiId age:age gender:gender];
    //data from Lite which is sent to
    //call flurry to update the imput values:::[UTILAnalyticsManager sharedInstance]
    
}

- (void)getKeys{
    [[[DFFeeds alloc] init] getKeys:_agencyLicenseKey responceHandler:^(MODELResultSet *resultSet) {
        if([resultSet hasError]){
            //show error
            dispatch_async( dispatch_get_main_queue(), ^{
                
                
                [self notifyNetworkStatus:2000];
                
            });
        }else{
            modelKeys = (MODELKeys*)resultSet.data;
            //NSMutableArray *keysArray = (NSMutableArray*)resultSet.data;
            //_kAccessKey     = modelKeys.aw(NSString*)[keysArray objectAtIndex:0] ;
            //_kSecretKey     = (NSString*)[keysArray objectAtIndex:1] ;
            //_kLicenseKey    = (NSString*)[keysArray objectAtIndex:2] ;
            [vapp setGivenLicenseKey:[modelKeys.vuforiaLicensekey UTF8String]];
            [vapp initAR:Vuforia::GL_20 orientation:self.interfaceOrientation];
           // [UTILAnalyticsManager startSessionWithAgencyId:@"agencyid" agencyName:@"agencyname"];
            // [UTILAnalyticsManager startSessionWithAgencyId:modelKeys.agencyId agencyName:modelKeys.agencyName ];
            [UTILAnalyticsManager  startSessionWithAgencyId:modelKeys.agencyId agencyName:modelKeys.agencyName flurryKey:modelKeys.flurryAppId ];  

        }
    }];
    
}
-(NSString*)modelKeys{
    return modelKeys.flurryAppId;
}
- (MODELKeys*)arKeys{
    return modelKeys;
}

- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    [viewRender registerStateChangeListener:stateChangeListener];
}
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    [viewRender registerStateChangeListener:stateChangeListener];
}

-(void)pauseVideoPlayerwithTrackableID:(NSString*)uniqueID{
    if(viewRender!=nil){
        for(id modelId in self.augmentsByTrackableID) {
            if(![modelId isEqualToString:uniqueID]){
                MODELAugment *augment = [self.augmentsByTrackableID objectForKey:modelId];
                [augment.mainContent.videoHelper pause];
                [augment.mainContent.videoHelper setShouldAutoPlay:YES];

                NSLog(@"Augments paused::::%@",augment);
                
            }
            
        }
    }
}

@end
