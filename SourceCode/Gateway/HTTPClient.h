//
//  WSWebServiceManager.h
//  Wowsome
//
//  Created by PIC on 04/02/16.
//  Copyright © 2016 PIC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

typedef void(^SuccessHandler) (id result,id resultString, BOOL apiStatus);
typedef void(^ErrorHandler) (NSError *error,NSString *errorMessage,BOOL apiStatus);
typedef NS_ENUM(NSUInteger, RequestType) {
    kPostMethod = 0,
    kGetMethod
};

@interface HTTPClient : NSObject

- (id)initWithURL:(NSString *)givenUrl withParameters:(NSDictionary *)givenParameters withRequestType:(RequestType)givenRequestType withCallBack:(SuccessHandler)givenSuccessHandler withErrorHandler:(ErrorHandler) givenErrorHandler;
- (void)start;

@end