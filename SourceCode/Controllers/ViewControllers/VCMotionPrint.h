/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

#import <UIKit/UIKit.h>
#import "ViewRender.h"
#import "SampleApplicationSession.h"
#import "MODELAugment.h"
#import "DLGMotionPrintCallback.h"
#import "MODELMedia.h"
#import <Vuforia/DataSet.h>
#import "MODELKeys.h"


@interface VCMotionPrint : UIViewController <SampleApplicationControl>
 {
    Vuforia::DataSet*  dataSet;
    BOOL fullScreenPlayerPlaying;
    
    BOOL scanningMode;
    BOOL isVisualSearchOn;
     UIAlertView *statusAlert;

    int lastErrorCode;

    // menu options
    BOOL extendedTrackingEnabled;
    BOOL continuousAutofocusEnabled;
    BOOL flashEnabled;
    BOOL playFullscreenEnabled;
    BOOL frontCameraEnabled;
}

- (id)initWithAgencyId:(NSString*)agencyId;
- (void)rootViewControllerPresentViewController:(UIViewController*)viewController inContext:(BOOL)currentContext;
- (void)rootViewControllerDismissPresentedViewController;
- (void) setVisualSearchOn:(BOOL) isOn ;
- (NSString *) lastTargetIDScanned;


- (VideoHelper*)videoHelper;
- (void) setLastTargetIDScanned:(NSString *) targetId;
+(UIImage*)getImage:(NSString*)name;
-(long)setTimer;
- (void) scanlineStart;
- (void) scanlineStop;
- (void)addCloseButton;
-(void)removeCloseButton;
-(NSString*)modelKeys;
- (MODELKeys*)arKeys;
-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender;
- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener;
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener;
- (void)addButtons;
-(void)removeButtons;
- (void)closeButtonTapped:(id)sender;

@property (nonatomic, strong) ViewRender* viewRender;
@property (nonatomic, strong) UITapGestureRecognizer * tapGestureRecognizer;
@property (nonatomic, strong) SampleApplicationSession * vapp;
@property (nonatomic, strong) NSString * lastTargetIDScanned;
@property (nonatomic, strong) MODELMedia *lastScannedBook;
@property (nonatomic, readwrite) BOOL showingMenu;
@property (nonatomic, weak) id<SampleGLResourceHandler> glResourceHandler;
@property (nonatomic, strong) NSString * trackableID;
@property (readwrite, nonatomic)long currentTimeMillis;
@property (readwrite, nonatomic)long loadingVideoDuration;
@property (readwrite, nonatomic)long initTimer;
@property (nonatomic, strong) UIButton *closeButton;
//@property (strong, nonatomic) VCFloatingActionButton *addButton;
@property (readwrite, nonatomic)BOOL isAugmentDataDownloaded;
-(void)pauseVideoPlayerwithTrackableID:(NSString*)uniqueID;

@end
