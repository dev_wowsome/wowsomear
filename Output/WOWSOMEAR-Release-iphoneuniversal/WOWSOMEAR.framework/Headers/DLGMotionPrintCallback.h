//
//  DLGMotionPrintCallback.h
//  WOWSOMEAR
//
//  Created by wowdev on 29/06/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#ifndef DLGMotionPrintCallback_h
#define DLGMotionPrintCallback_h

typedef enum {
    AR_INIT_SUCCESS,
    AR_INIT_FAILED
} EVENT_TYPE;


#endif /* DLGMotionPrintCallback_h */
@protocol DLGMotionPrintCallback

@required
// this method is called to notify the application that the initialization (initAR) is complete
// usually the application then starts the AR through a call to startAR
- (void) onMotionPrintEvent:(EVENT_TYPE)eventType data:(id)data;

@end

