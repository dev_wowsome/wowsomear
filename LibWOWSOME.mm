//
//  WOWSOMEManager.m
//  WOWSOMEAR
//
//  Created by wowsome on 14/06/16.
//  Copyright © 2016 WOWSOME. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LibWOWSOME.h"
#import "VCMotionPrint.h"
#import "WOWSOMEFactoryDelegate.h"


@interface LibWOWSOME(){
    VCMotionPrint *vcMotionPrint;
    NSString *agencyId;
}



@end

@implementation LibWOWSOME
+ (id<WOWSOMEFactoryDelegate>) getInstance:(NSString*)agencyId  {
    static id<WOWSOMEFactoryDelegate> factoryDelegate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        factoryDelegate = [[[self class] alloc] init];
        ((LibWOWSOME*)factoryDelegate)->agencyId = agencyId;
    });
    
    return factoryDelegate;
}



- (UIViewController*) getInteracativePrintController{
    if(vcMotionPrint==nil) vcMotionPrint = [[VCMotionPrint alloc] initWithAgencyId:agencyId];
    NSLog(@"getinteractivePRint ::::%@::::%@",agencyId,vcMotionPrint);
    return vcMotionPrint;
}



-(void)updateUserEmailId:(NSString*)emaiId age:(int)age gender:(NSString*)gender{
    [vcMotionPrint updateUserEmailId:emaiId age:age gender:gender];
}

- (void)registerStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    [vcMotionPrint registerStateChangeListener:stateChangeListener];
}
- (void)unregisterStateChangeListener:(id<StateChangeListener>)stateChangeListener{
    [vcMotionPrint registerStateChangeListener:stateChangeListener];
}

-(void) dealloc{
    vcMotionPrint = nil;
}


// class LibWOWSOME implements WOWSOMEFactoryDelegate {
//    private FRGMTMotionPrint frgmtMotionPrint;
//    private List<StateChangeListener> stateChangeListeners;
//    private LibWOWSOME(){}
//    @Override
//    public WOWSOMEFactoryDelegate newInstance() {
//        return new LibWOWSOME();
//    }
//    @Override
//    public Fragment getInteractivePrintFragment(){
//        if(frgmtMotionPrint==null)frgmtMotionPrint = new FRGMTMotionPrint();
//        return frgmtMotionPrint;
//    }
//    @Override
//    public void updateUserData(String email, int age, String gender) {
//        if(frgmtMotionPrint!=null){
//            frgmtMotionPrint.updateUserData(email, age, gender);
//        }
//    }
//    @Override
//    public void registerStateChangeListener(StateChangeListener stateChangeListener) {
//        if(stateChangeListeners==null)stateChangeListeners = new ArrayList<StateChangeListener>();
//        stateChangeListeners.add(stateChangeListener);
//    }
//    @Override
//    public void unregisterStateChangeListener(StateChangeListener stateChangeListener) {
//        if(stateChangeListeners!=null) {
//            stateChangeListeners.remove(stateChangeListener);
//        }
//    }
//}

@end
